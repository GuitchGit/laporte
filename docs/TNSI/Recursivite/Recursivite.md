=== "Cours"
  
    !!! note "Langages et programmation[![](img/BO_Recursivite.png){ align=right}](../recursivite_cours)"

=== "Exercices"
    ??? note "Exercice 1 : Somme des élèments d'un tableau"
        On souhaite réaliser la somme des éléments du tableau suivant: tab = [1,2,3,4,5,6,7,8,9,10] ce qui donne 55

        Voici une version itérative:
        ```python
        tab = [1,2,3,4,5,6,7,8,9,10]
        
        def sommeIt(L):
            somme=0
            for valeur in L:
                somme+= valeur
            return somme

        print(sommeIt(tab))
        ```

        Faire un essai :

        {{IDE()}}

        Proposer une version récursive nommée sommeRec(L)

        ??? done "Solution"

            Il faut chercher! :smile:

            <!--
            ```python
            tab = [1,2,3,4,5,6,7,8,9,10]
            def sommeRec(L):
                if len(L)==1:
                    return L[0]
                else : 
                    return L[0] + sommeRec(L[1:])
            
            print(sommeRec(tab))
            ```
            -->


    ??? note "Exercice 2 : Fonction miroir et Palindrome"
        
        A venir
    
    ??? note "Exercice 4 : C'est moi la star"
        On souhaite rédiger une fonction `star1(n)` qui prend pour argument un entier naturel n $\in$ $\mathbb{N}$ et affiche dans la console n étoiles sur la première ligne, n-1 sur la seconde ligne, . . ., et enfin une étoile sur la n$^{ième}$ ligne.
        
        ![](../img/star1.png){width=20%}
        

        Il est bien entendu très simple de rédiger une version itérative de cette fonction, mais on peut aussi observer qu’il ne s’agit finalement, lorsque n est strictement positif, que de tracer n étoiles sur la première ligne puis d’appliquer star1(n−1). Traduit en Python, cela donne :

        ```python
        def star1(n):
            if n == 1:
                print('*')
            else:
                print(n*'*')
                star1(n-1)

        star1(5)
        ```
        __^^Remarque^^__ : vous pouvez mettre un

        __print('\n')__

        avant la fonction si vous ne voulez pas de décalage sur la première ligne. C'est juste un problème d'affichage dû à l'IDE en ligne. Il n'est pas nécessaire sur Jupyter par exemple.

        On obtient:

        ![](../img/star1_affichage.png)

        # Comment fonctionne les appels récursifs :
        
        Avec PythonTutor : __cliquer__ [ici](https://urlz.fr/iHnU){target=__blank}
                
        - 1$^{er}$ appel de la fonction :  n=5, donc n!=1 --> on affiche (5*''*') 
        - 2$^{ième}$ appel de la fonction : n=4, donc n!=1 --> on affiche (4*''*')
        - 3$^{ième}$ appel de la fonction : n=3, donc n!=1 --> on affiche (3*''*')
        - 4$^{ième}$ appel de la fonction : n=2, donc n!=1 --> on affiche (2*''*')
        - 5$^{ième}$ appel de la fonction : n=1, donc n=1  --> on affiche ('*')

        __a. Rédiger__ une fonction récursive __`star2(n)`__ qui prend pour argument un entier n et qui affiche une étoile sur la première ligne, deux sur la seconde, . . . et enfin n sur la n$^{ième}$

        On doit obtenir :

        ![](../img/star2.png){width=20%}

        {{IDE()}}







    
=== "Exercices Type BAC" 
    !!! note "[Chaîne de caractères 🔠](../recursivite_Type_Bac1)"
        <!-- Sujet Polynésie 2022 - J1 - Ref : 22-NSIJ1PO1 -->

    !!! note "[Récursibvité .... A compléter](../recursivite_Type_Bac2)"
        <!-- Sujet métropole 2022 - J2 - Ref : 22-NSIJ2ME1 -->  

=== "Ressources"   
    
    Sur Lumni :
        
    - [La récursivité](https://www.lumni.fr/video/une-introduction-a-la-recursivite){target=__blank}

    - Faire apparaître les appels récursif sous python
        Exemple : 

        ``` python
            import rcviz

            cg = rcviz.CallGraph()
            @rcviz.viz(cg)

            def star1(n):
                if n == 0:
                    print('*')
                else:
                    print(n*'*')
                    star1(n-1)

            star1(5)
            cg.render()
        ```


        



[📄 Accueil](../../../){ .md-button }
