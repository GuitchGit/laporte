[ ⬅️ Retour](../Recursivite){ .md-button }

??? video

    |__Une introduction à la récursivité__||
    |:---:|:---:|
    |<iframe width="400" height="256" src="//embedftv-a.akamaihd.net/f05decdeb3cb0d3e92e0ee9eb8b78977" frameborder="0" scrolling="no" allowfullscreen></iframe>||
    |Tour de Hanoï (En anglais) Mr Thorsten Altenkirch||
    |<iframe width="400" height="256" src="https://www.youtube.com/embed/8lhxIOAfDss" title="Recursion 'Super Power' (in Python) - Computerphile" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>||

![](../img/vache111.png){width=50%}![](../img/miroir.png){width=45%}

???+ note "__`1. Définition`__"

    __Une fonction récursive est une fonction qui s'appelle elle-même. Nous parlons alors d'appel récursif.__

    ??? info "Pour aller plus loin..."
        - Définition récurrente d’un ensemble :
                Description des éléments de base
                        +
                Règles de construction récursive
        - Les règles de construction récursives permettent de caractériser les éléments d'un ensemble à partir d'éléments plus simples du même ensemble (en général obtenus ou construits à l'étape précédente). 
        - Il y a toujours un (ou plusieurs) éléments de base.
        - En informatique les traitements récursifs doivent s ’appliquer à partir d’un modèle récursif cohérent.
        - S'il s'agit d'une méthode de résolution, le problème à résoudre doit se décomposer en sous-problèmes de même nature, et ce jusqu'à une formulation elémentaire (problème de base).
        - Dans les langages de programmation la récursivité est la possibilité donnée à une fonction (ou une procédure) à s’appeler (se rappeler) elle-même.
        - Les schémas de programmation qui en découlent sont en général plus simples et plus concis.


    __De façon humouristique__ :

    - __^^Exemple 1^^__ :
        ```python
            def marcher()
                mettre_un_pied_devant_l_autre()
                marcher()
        ```
    - __^^Exemple 2^^__ :

        Si vous n'avez pas encore compris ce qu'est une fonction récursive : Relisez cette phrase depuis le début.

    __^^Attention^^__ : 
    dans ces exemples il manque un ou des __"test(s) ou une ou des condition(s) d'arrêt(s)"__ ou encore appelé __"cas de base(s)"__ (Voir après)

    __^^Remarque^^__ : 
    On a en général une consommation mémoire à l’exécution plus importante et un temps de traitement plus long. (Voir suite de Fibonacci)


    # __`2. Mise en évidence de la nécessité d'un cas d'arrêt`__

    Soit la fonction suivante:
        ```python
            def prems()
                print("Un très mauvais exemple")
                prems()
        ```

    Si on appelle cette fonction par la commande:
    ```python
        >>> prems()
    ```

    On obtient :
    ```python
        >>> Un très mauvais exemple
        >>> Un très mauvais exemple
        >>> Un très mauvais exemple
        >>> Un très mauvais exemple
        >>> Un très mauvais exemple
        >>> ...
    ```
    ???+ tldr "__Conclusion__"
        Évidemment, comme prévu, ce programme ne s'arrête pas. Nous sommes obligés de l'arrêter manuellement. Nous sommes (volontairement) tombés dans un piège qui sera systématiquement présent lors d'une programmation récursive :
        
        __le piège de la boucle infinie__.







  
[ ⬅️ Retour](../Recursivite){ .md-button }
[📄 Accueil](../../../){ .md-button }