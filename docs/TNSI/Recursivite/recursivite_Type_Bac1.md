[ ⬅️ Retour](../Recursivite){ .md-button }

# __`Gestion chaîne de caractère`__

On rappelle qu'une chaîne de caractères peut être représentée en Python par un texte entre guillemets " " et que :

- la fonction __`len`__ renvoie la longueur de la chaîne de caractères passée en paramètre ;
- si une variable __ch__ désigne une chaîne de caractères, alors __ch[0]__ renvoie son premier caractère, ch[1] le deuxième, etc. ;
- l'opérateur __+__ permet de concaténer deux chaînes de caractères.

## Exemples:

```python
>>> texte = "bricot"
>>> len(texte)
6
>>> texte[0]
"b"
>>> texte[1]
"r"
>>> "a" + texte
"abricot"
```

On s'intéresse dans cet exercice à la construction de chaînes de caractères suivant certaines règles de construction.

!!! done "__Règle A__" 
    Une chaîne est construite suivant la __règle A__ dans les deux cas suivants:

    - soit elle est égale à __"a"__ ;
    - soit elle est de la forme __"a"+chaine+"a"__, où chaine est une chaîne de caractères construite suivant la __règle A__.

!!! done "__Règle B__"
    Une chaîne est construite suivant la __règle B__ dans les deux cas suivants :

    - soit elle est de la forme __"b"+chaine+"b"__, où chaine est une chaîne de caractères construite suivant la __règle A__;
    - soit elle est de la forme __"b"+chaine+"b"__, où chaine est une chaîne de caractères construite suivant la __règle B__.


!!! tip "On a reproduit ci-dessous l'aide de la fonction choice du module random" 
    ```python
    >>> from random import choice
    >>> help(choice)
    Help on method choice in module random:
    choice(seq) method of random.Random instance
    Choose a random element from a non-empty sequence.
    ```
    `choice(seq)` renvoie un élément de `seq` (qui peut être une liste) de façon pseudo-aléatoire.

# PARTIE 1 :
__1.a. Tester__ le programme suivant 

{{ IDE('test') }}

__Expliquer__ en une ou deux phrase(s) le fonctionnement de l'expression conditionnelle
??? done "Solution"

    Il faut chercher! :smile:
        
    <!--La méthode __"choice"__ de la bibliothèque __Random__ choisit aléatoirement dans le tableau [True, False] soit la valeur True soit la valeur False.
    - Si "True" est choisi", c'est la structure "if" qui est exécutée
    - Si "False" est choisi, c'est la structure "else" qui est exécutée.
    -->

La fonction __`A()`__ ci-dessous renvoie une chaîne de caractères construite suivant la __règle A__, en choisissant aléatoirement entre les deux cas de figure de cette règle.

```python
def A():
    if choice([True, False]):
        return "a"
    else:
        return "a" + A() + "a"
```
__1.b.__ Cette fonction est-elle récursive? __Justifier.__

??? done "Solution"

    Il faut chercher! :smile:
        
    <!--La fonction A s'appelle elle-même, donc A est une fonction récursive.-->


__1.c.__ La fonction __`choice([True, False])`__ peut renvoyer __False__ un très grand nombre de fois consécutives.

__Expliquer__ pourquoi ce cas de figure amènerait à une erreur d'exécution.

??? done "Solution"

    Il faut chercher! :smile:
        
    <!--Si choice([True, False]) renvoie False consécutivement un nombre de fois supérieur à la limite de profondeur de récursion autorisée (1000 par défaut avec Python), dans ce cas une erreur d'exécution se produit.-->

    <!-- 
        !!! danger "Pour aller plus loin"
            On pourrait modifier cette limite à $10^6$ avec le code suivant
            ```python
            import sys
            sys.setrecursionlimit(10**6)
            ```
    -->

# PARTIE 2 :
Dans la suite, on considère une deuxième version de la fonction A.

À présent, la fonction prend en paramètre un entier __`n`__ tel que:
- si la valeur de __`n`__ est négative ou nulle, la fonction renvoie __"a"__;
- si la valeur de __`n`__ est strictement positive, elle renvoie une chaîne de caractères construite suivant la __règle A__ avec un __`n`__ décrémenté de __1__, en choisissant aléatoirement entre les deux cas de figure de cette règle.

```python
def A(n):
    if ... or choice([True, False]) :
        return "a"
    else:
    return "a" + ... + "a"
```

__2.a. Recopier__ sur la copie et compléter aux emplacements des points de suspension `...` le code de cette nouvelle fonction __`A`__.

??? done "Solution"

    Il faut chercher! :smile:
      
    <!--Si __`choice([True, False])`__ renvoie False consécutivement un nombre de fois supérieur à la limite de profondeur de récursion autorisée (1000 par défaut avec Python), dans ce cas une erreur d'exécution se produit.
    ```python
    def A(n):
        if (n <= 0) or choice([True, False]) :
            return "a"
        else:
            return "a" + A(n - 1) + "a"
    ```
    -->
    

__2.b.Justifier__ le fait qu'un appel de la forme `A(n)` avec `n` un nombre entier positif inférieur à 50, termine toujours.
    
??? done "Solution"

    Il faut chercher! :smile:
        
    <!--Pour $n > 0$, l'appel à `A(n)` provoque ou bien un arrêt de la fonction, ou bien un appel récursif avec le paramètre `n - 1`.-->
    
    <!--Un appel à `A(50)` pourrait provoquer dans le pire des cas 50 appels récursifs pour arriver à `A(0)` qui termine, ou alors terminer avant !-->
       
            
# PARTIE 3 :

On donne ci-après le code de la fonction récursive B qui prend en paramètre un entier `n` et qui renvoie une chaine de caractères construite suivant la règle B.

```python
def B(n):
    if (n <= 0) or choice([True, False]):
        return "b" + A(n - 1) + "b"
    else:
        return "b" + B(n - 1) + "b"
```

On admet que :

- les appels `A(-1)` et `A(0)` renvoient la chaine `"a"` ;
- l'appel `A(1)` renvoie la chaine `"a"` ou la chaine `"aaa"` ;
- l'appel `A(2)` renvoie la chaine `"a"`, la chaine `"aaa"` ou la chaine `"aaaaa"`.
        
__3. Donner__ toutes les chaines possibles renvoyées par les appels `B(0)`, `B(1)` et `B(2)`.

??? done "Solution"

    Il faut chercher! :smile:
        
    <!--
    1. `B(0)` renvoie `"bab"`
        ??? tip "__^^Complément d'explication^^__"
            Comme n=0, la condition __`if n<=0`__ est vraie. La fonction renvoie donc __`"b" + A(0-1) + "b"`__ = __`"b" + A(-1) + "b"`__
            ^^Or^^ : A(-1) = "a" donc `B(0)` renvoie `"bab"`
            Finalement, il y a 1 cas:
            ### `B(0)` renvoie `"bab"`
    2. `B(1)` renvoie `"bab"` ou `"bbabb"`.
        ??? tip "__^^Complément d'explication^^__ "
            ###__a__.
            Comme n=1, la condition __`if n<=0`__ est fausse mais Choice peut renvoyer "True" dans ce cas, on renvoie : __"b" + A(n - 1) + "b"__
            On revient dans le cas précédent, donc `B(1)` renvoie `"bab"`
            ###__b__.
            Comme n=1, la condition __`if n<=0`__ est fausse mais Choice peut renvoyer "False" dans ce cas, on renvoie : __"b" + B(n - 1) + "b"__
            ^^donc^^ : __"b" + B(1 - 1) + "b"__ = __"b" + B(0) + "b"__
            ^^Or^^ : B(0) = "bab" donc `B(1)` renvoie `"b`__`bab`__`b"`
            Finalement, il y a 2 cas:
            ### `B(1)` renvoie `"bab"` ou `"bbabb"`.
    3. `B(2)` renvoie `"bab"`, `"baaab"`, `"bbabb"` ou `"bbbabbb"`
        ??? tip "__^^Complément d'explication^^__" 
            ###__a__.
            Comme n=2, la condition __`if n<=0`__ est fausse mais Choice peut renvoyer "True" dans ce cas, on renvoie : __"b" + A(n - 1) + "b"__
            On obtient : __"b" + A(2 - 1) + "b"__ = __"b" + A(1) + "b"__
            ^^or^^ :  A(1) = "a" ou "aaa"
            ^^Donc^^: `B(2)` renvoie `"b`__a__`b"` ou `"b`__aaa__`b"`
            ###__b__.
            Comme n=2, la condition __`if n<=0`__ est fausse mais Choice peut renvoyer "False" dans ce cas, on renvoie : __"b" + B(n - 1) + "b"__
            On obtient : __"b" + B(2 - 1) + "b"__ = __"b" + B(1) + "b"__
            ^^Or^^ : B(1) = `"bab"` ou `"bbabb"`
            Donc `B(2)` renvoie `"b`__bab__`b"` ou `"b`__bbabb__`b"`
            Finalement, il y a 4 cas:
            ### `B(2)` renvoie `"bab"`, `"baaab"`, `"bbabb"` ou `"bbbabbb"  
    -->








    

    
   

# PARTIE 4 :    
On suppose maintenant qu'on dispose d'une fonction `raccourcir` qui prend comme paramètre une chaine de caractères de longueur supérieure ou égale à 2, et renvoie la chaine de caractères obtenue à partir de la chaine initiale en lui ôtant le premier et le dernier caractère.

!!! example "Par exemple :"

    ```pycon
    >>> raccourcir("abricot")
    "brico"
    >>> raccourcir("ab")
    ""
    ```

__4.a. Recopier__ sur la copie et compléter les points de suspension `...` du code de la fonction `regle_A` ci-dessous pour qu'elle renvoie `True` si la chaine passée en paramètre est construite suivant la règle A, et `False` sinon.

```python
def regle_A(chaine):
    n = len(chaine)
    if n >= 2:
        return (chaine[0] == "a") and (chaine[n - 1] == "a") and regle_A(...)
    else:
        return chaine == ...
```

??? done "Solution"

    Il faut chercher! :smile:
        
    <!--```python
    def regle_A(chaine):
        n = len(chaine)
        if n >= 2:
            return (chaine[0] == "a") and (chaine[n - 1] == "a") and regle_A(raccourcir(chaine))
        else:
            return chaine == "a"
    ```
    -->   
    

__4.b. Écrire__ le code d'une fonction `regle_B`, prenant en paramètre une chaine de caractères et renvoyant `True` si la chaine est construite suivant la règle B, et `False` sinon.

??? done "Solution"

    Il faut chercher! :smile:
        
    <!--```python
    def regle_B(chaine):
        n = len(chaine)
        if n >= 2:
            return (chaine[0] == "b") and (chaine[n - 1] == "b") and (
                regle_A(raccourcir(chaine)) or regle_B(raccourcir(chaine))
            )
        else:
            return False
    ```
    -->   
    
    
















[ ⬅️ Retour](../Recursivite){ .md-button }
[📄 Accueil](../../../){ .md-button }