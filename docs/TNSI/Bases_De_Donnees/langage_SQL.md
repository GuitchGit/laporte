[ ⬅️ Retour](../Bases_De_Donnees){ .md-button }

[L'essentiel du langage SQL](data/SQL_L_Essentiel.pdf){:target="_blank"}

Dans toute la suite, les manipulations sont à faire en interrogeant la base de données `livres.db`, avec l'une des méthodes indiquées ci-dessus.
        
Cette base de données contient les tables suivantes :
        
![](img/Base_Donnees_Livres.png)
        
### 1.1. Sélection de données
??? note "Exemple 1 - Requête basique : SELECT, FROM, WHERE"

    - **Commande :** 
        ```sql
        SELECT titre
        FROM livre
        WHERE annee >= 1990;
        ``` 
    - **Exécution:** 

    On veut les titres de la table «livre» qui sont parus après (ou en ) 1990;

    Remarque : 112 eregistrements doivent apparaître!

    {!{ sqlide titre="Tape ta requête SQL ici :" init="BDD1/livres.sql"}!}



??? note "Exemple 2 - Requête avec booléen : AND"

    - **Commande :** 
        ```sql
        SELECT titre
        FROM livre
        WHERE   annee >= 1970 AND annee <= 1980 AND editeur = 'Dargaud';
        ``` 
    
    - **Exécution:**

    On veut les titres de la table «livre» qui sont parus entre 1970 et 1980 chez l'éditeur Dargaud;

    {!{ sqlide titre="Tape ta requête SQL ici :" init="BDD1/livres.sql"}!}
    
??? note "Exemple 3 - "Requête approchée : LIKE"

    - **Commande :** 
        ```sql
        SELECT titre
        FROM livre
        WHERE titre LIKE '%Astérix%';
        ``` 
    - **Exécution:**

    On veut les titres de la table «livre» dont le titre contient la chaîne de caractères "Astérix".  
    Le symbole ```%``` est un joker qui peut symboliser n'importe quelle chaîne de caractères. 
    
    {!{ sqlide titre="Tape ta requête SQL ici :" init="BDD1/livres.sql"}!}
    
??? note "Exemple 4 - Plusieurs colonnes"
    - **Commande :** 
        ```sql
        SELECT titre, isbn
        FROM livre
        WHERE annee >= 1990;
        ```

    - **Exécution :** 
    
    On veut les titres et les ISBN de la table «livre» qui sont parus après 1990.

    {!{ sqlide titre="Tape ta requête SQL ici :" init="BDD1/livres.sql"}!}


??? note "Exemple 5 - Toutes les colonnes : * "
    - **Commande :** 
        ```sql
        SELECT *
        FROM livre
        WHERE annee >= 1990;
        ``` 
        
    - **Exécution :**
        
        On veut toutes les colonnes disponibles de la table «livre» pour les livres qui sont parus après 1990. 
        L'astérisque ```*``` est un joker (*wildcard* en anglais).

        {!{ sqlide titre="Tape ta requête SQL ici :" init="BDD1/livres.sql"}!}
            
??? note "Exemple 6 - Renommer les colonnes : AS "
    - **Commande :** 
        ```sql
        SELECT titre AS titre_du_livre
        FROM livre
        WHERE annee >= 1990;
        ``` 
        Lors de l'affichage du résulats et dans la suite de la requête (important), la colonne "titre" est renommée "titre_du_livre".

        {!{ sqlide titre="Tape ta requête SQL ici :" init="BDD1/livres.sql"}!}

### 1.2. Opérations sur les données : sélection avec agrégation
        
??? note " Exemple 7 - Compter : COUNT "
    - **Commande :** 
        ```sql
        SELECT COUNT(*) AS total
        FROM livre
        WHERE titre LIKE "%Astérix%";
        ``` 
    - **Execution :**

        On veut compter le nombre d'enregistrements de la tables livres comportant le mot "Astérix". Le résultat sera le seul élément d'une colonne nommée «total». 
            
        {!{ sqlide titre="Tape ta requête SQL ici :" init="BDD1/livres.sql"}!}

??? note " Exemple 8 - Additionner : SUM "
    - **Commande :** 
        ```sql
        SELECT SUM(annee) AS somme
        FROM livre
        WHERE titre LIKE "%Astérix%";
        ``` 
    - **Execution :** 

        On veut additionner les années des livres de la tables livres comportant le mot "Astérix". Le résultat sera le seul élément d'une colonne nommée «somme».
        *Attention : dans notre cas précis, ce calcul n'a aucun sens...*
            
        {!{ sqlide titre="Tape ta requête SQL ici :" init="BDD1/livres.sql"}!}
    
??? note " Exemple 9 - Faire une moyenne : AVG "
    - **Commande :** 
        ```sql
        SELECT AVG(annee) AS moyenne
        FROM livre
        WHERE titre LIKE "%Astérix%";
        ``` 
            
    - **Execution :** 

        On veut calculer la moyenne des années de parution des livres de la table livres comportant le mot "Astérix". Le résultat sera le seul élément d'une colonne nommée «moyenne».
            
        {!{ sqlide titre="Tape ta requête SQL ici :" init="BDD1/livres.sql"}!}

??? note " Exemple 10 - Trouver les extremums : MIN, MAX "
    - **Commande :** 
        ```sql
        SELECT MIN(annee) AS minimum
        FROM livre
        WHERE titre LIKE "%Astérix%";
        ``` 
            
    - **Execution :** 

        On veut trouver la pus petite valeur de la colonne «annee» parmi les livres de la tables livre comportant le mot "Astérix". Le résultat sera le seul élément d'une colonne nommée minimum. Le fonctionnement est identique avec **MAX** pour la recherche du maximum.

        {!{ sqlide titre="Tape ta requête SQL ici :" init="BDD1/livres.sql"}!}


??? note " Exemple 11 - Classer des valeurs : ORDER BY, ASC, DESC "
    - **Commande :** 
        ```sql
        SELECT titre, annee
        FROM livre
        WHERE titre LIKE "%Astérix%"
        ORDER BY annee DESC;
        ``` 
            
    - **Execution :** 

        On veut afficher tous les albums d'Astérix, et leur année de parution, classés par année décroissante.

        {!{ sqlide titre="Tape ta requête SQL ici :" init="BDD1/livres.sql"}!}

??? note " Exemple 12 - Suppression des doublons : DISTINCT "
    - **Commande :** 
        ```sql
        SELECT DISTINCT editeur
        FROM livre;
        ``` 
            
    - **Execution :** 

        On veut la liste de tous les éditeurs. Sans le mot-clé ```DISTINCT```, beaucoup de doublons apparaîtraient.

        {!{ sqlide titre="Tape ta requête SQL ici :" init="BDD1/livres.sql"}!}
        
## 1.3 Des recherches croisées sur les tables : les jointures

??? note "Observation"
    Observons le contenu de la table «emprunt» :
            
        ```sql
        SELECT *
        FROM emprunt;
        ``` 
            
        {!{ sqlide titre="Tape ta requête SQL ici :" init="BDD1/livres.sql"}!}

        Le contenu est peu lisible : qui a emprunté quel livre ?  
        Souvenons-nous du scheam relationnel de la base de données.
        ![](img/Base_Donnees_Livres.png)

        Pour que la table «emprunt» soit lisible, il faudrait (dans un premier temps) que l'on affiche à la place de l'ISBN le titre de l'ouvrage.
            
        Or, ce titre est disponible dans la table «livres».
        On va donc procéder à une **jointure** de ces deux tables.

??? note " Exemple 13 - Jointure de 2 tables : JOIN "
    - **Commande :** 
        ```sql
        SELECT livre.titre, emprunt.code_barre, emprunt.retour
        FROM emprunt
        JOIN livre ON emprunt.isbn = livre.isbn;
        ``` 
            
    - **Execution :** 

        Comme plusieurs tables sont appelées, nous préfixons chaque colonne avec le nom de la table. Nous demandons ici l'affichage de la table «emprunt», mais où on aura remplacé l'ISBN (peu lisible) par le titre du livre.
            
        {!{ sqlide titre="Tape ta requête SQL ici :" init="BDD1/livres.sql"}!}

        L'expression
        ```sql
        JOIN livre ON emprunt.isbn = livre.isbn
        ``` 
        doit se comprendre comme ceci : on «invite» la table «livres» (dont on va afficher la colonne «titre»). La correspondance entre la table «livres» et la table «emprunt» doit se faire sur l'attribut ISBN, qui est la clé primaire de «livres» et une clé étrangère d'«emprunts».  
        Il est donc très important de spécifier ce sur quoi les deux tables vont se retrouver (ici, l'ISBN) 
    
??? note " Exemple 14 - Jointure de 3 tables : JOIN "

    Le résultat précédemment a permis d'améliorer la visibilité de la table «emprunt», mais il reste la colonne «code_barre» qui est peu lisible. Nous pouvons la remplacer par le titre du livre, en faisant une nouvelle jointure, en invitant maintenant les deux tables «livre» et «usager».
    
    - **Commande :** 
        ```sql
        SELECT u.nom, u.prenom, l.titre, e.retour FROM emprunt AS e
        JOIN livre AS l ON e.isbn = l.isbn
        JOIN usager AS u ON e.code_barre = u.code_barre;
        ``` 
            
    - **Execution :** 

    Il faut bien comprendre que la table principale qui nous intéresse ici est «emprunts», mais qu'on modifie les valeurs affichées en allant chercher des correspondances dans deux autres tables.
    Notez ici que des alias sont donnés aux tables (par **AS**) afin de faciliter l'écriture. 

    {!{ sqlide titre="Tape ta requête SQL ici :" init="BDD1/livres.sql"}!}


### 1.4 Mise à jour, insertion et suppression d'enregistrements
    
On ne peut pas insérer, supprimer ou mettre à jour sur la base de données précédentes en ligne.
    
Pour cela, on va créer une nouvelle base de données en utilisant SQlite.
    
__Ouvrir__ Edupython et __cliquer__ sur l'icône :  ![](img/Edupython_Base_Donnees.png)

__Ouvrir__ une nouvelle base de données :

![](img/Nvelle_Base_Donnees.png)

Une fenêtre s'ouvre, enregistrer ce fichier sous : __`films.db`__

__Aller__ sur l'onglet : ![](img/executer_SQL.png)

Reamrque : Toutes les requêtes SQL se feront dans cette fenêtre!

La création des tables ne fait pas partie du programme de NSI, mais voici les commandes que nous utiliserons pour nos exemples :
```sql
CREATE TABLE "Realisateurs" (
    "real" INTEGER NOT NULL,
    "nom" TEXT,
     PRIMARY KEY("real")
);

CREATE TABLE "Films" (
    "film" INTEGER NOT NULL,
    "titre" TEXT NOT NULL,
    "real" INTEGER,
    "annee" INTEGER,
    "duree" INTEGER,
    PRIMARY KEY("film"),
    FOREIGN KEY("real") REFERENCES "Realisateurs"("real")
);
```

On peut voir que les types des attributs sont bien indiqués, ainsi que les clefs primaires et étrangères. Il y a également des contraintes de domaine pour les valeurs qui ne peuvent pas être vides.

??? note " Exemple 15 - Insertion d'un nouvel enregistrement : INSERT INTO"

    On souhaite insérer un nouvel enregistrement la table `Réalisateurs`  

    Pour insérer des valeurs dans une table, il faut utiliser la commande :
    ```sql
        INSERT INTO nom_table
        VALUES(val1, val2, ..., valN);
    ```    
    
    - **Commande :** 
        ```sql
        INSERT INTO "Realisateurs" VALUES(1, "Stanley Kubrick");
        INSERT INTO "Realisateurs" VALUES(2, "Ridley Scott");
        INSERT INTO "Realisateurs" VALUES(3, "Katsuhiro Otomo");
        ```

        ```sql
        INSERT INTO "Films" VALUES(1, "2001: A Space Odyssey", 1, 1968, 149);
        INSERT INTO "Films" VALUES(2, "Alien", 2, 1979, 117);
        INSERT INTO "Films" VALUES(3, "Blade Runner", 2, 1982, 117);
        INSERT INTO "Films" VALUES(4, "Akira", 3, 1988, 124);
        ``` 

    - **Réaliser quelques vérifications**      
     **Vérification:** 
        ```sql
        SELECT * 
        FROM Realisateurs
        ``` 

        ```sql
        SELECT titre, annee 
        FROM Films
        ``` 

        ```sql
        SELECT duree 
        FROM Films
        ``` 
        On peut remarquer que dans le dernier exemple, il y a plusieurs lignes identiques. En effet, SELECT peut s’affranchir de la contrainte de l’algèbre relationnelle interdisant les tuples identiques. En rajoutant SELECT DISTINCT duree FROM Films, on obtient des lignes toutes différentes!

        ```sql
        SELECT DISTINCT duree 
        FROM Films
        ``` 

??? note " Exemple 16 - Mise à jour : UPDATE "
    
    Pour changer un ou des attributs d’un tuple déjà défini, on peut utiliser l’expression suivante:
    
    ```sql
        UPDATE nom_table
        SET attribut1=val1, ..., attributN=valeurN
        WHERE condition;
    ``` 

    Pour changer la durée du film Blade Runner, il faut utiliser :

    - **Modification** 
        ```sql
        UPDATE Films
        SET duree = 120
        WHERE titre = "Blade Runner";
        ``` 
            
    - **Vérification**

    __Saisir__ une requête SQL pour vérifier votre mise à jour

??? note " Exemple 17 - Suppression d'un enregistrement : DELETE "
    
    Pour supprimer certaines lignes d’une table, il faut faire : 
    
    ```sql
        DELETE FROM nom_table
        WHERE condition;
    ``` 

    Ainsi, pour supprimer Ridley Scott des réalisateurs, on doit utiliser :

    - **Modification** 
        ```sql
        DELETE FROM Realisateurs
        WHERE nom="Ridley Scott"
        ``` 
            
    Que constatez-vous? Proposer une solution
    
    ??? done "Solution"

        Il faut chercher! :smile:
        
        <!-- Puisque la table Films utilise une clef étrangère provenant de Realisateurs, supprimer un réalisateur qui apparaît dans Films ne respecte pas les contraintes d’intégrité. Il faut alors supprimer ses films avant de supprimer le réalisateur. -->
    
        


### 1.5 Exercice d'application : The SQL Murder Mystery 
Cet exercice en ligne est proposé le Knight Lab de l'université américaine Northwerstern University.

**Le point de départ de l'histoire** : un meurtre a été commis dans la ville de SQL City le 15 janvier 2018.

 À partir de ce point de départ et d'une base de données dont le diagramme est donné ci-dessous, il s'agit de trouver le meurtrier.

![](../img/schemaMM.png){: .center width=100%}
        
Rendez-vous sur [cette page](https://mystery.knightlab.com/walkthrough.html){:target="_blank"}, et bonne enquête à coups de requêtes !

- Vous pouvez travailler en ligne ou bien dans votre SGBD préféré, avec la base [sql-murder-mystery.db](../data/sql-murder-mystery.db). Attention pour valider votre réponse, il faudra vous rendre en bas de la page officielle.

- Vous pouvez trouver des éléments de correction [ici](../data/solution.txt){:target="_blank"}.

[ ⬅️ Retour](../Bases_De_Donnees){ .md-button }
[📄 Accueil](../../../){ .md-button }