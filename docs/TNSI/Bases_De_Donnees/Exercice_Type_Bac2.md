[ ⬅️ Retour](../Bases_De_Donnees){ .md-button }

# __`Et que vive la musique!`__

![](img/Exercices/Musicien.png){width=15% align=left}
L’énoncé de cet exercice utilise les mots clefs du langage SQL suivants : __SELECT__, __FROM__, __WHERE__, __JOIN ON__, __UPDATE__, __SET__, __INSERT INTO VALUES__, __COUNT__, __ORDER BY__.

 - La clause __ORDER BY__ suivie d'un attribut permet de trier les résultats par ordrecroissant de l'attribut ;

 - __COUNT(*)__ renvoie le nombre de lignes d'une requête ;



Un musicien souhaite créer une base de données relationnelle contenant ses morceaux et interprètes préférés. Pour cela il utilise le langage SQL.

Il crée une table morceaux qui contient entre autres les titres des morceaux et leur année de sortie :

|id_morceau|titre|annee|id_interprete|
|:---:|:---:|:---:|:---:|
|1|Like a Rolling Stone|1965|1|
|2|Respect|1967|2|
|3|Imagine|1970|3|
|4|Hey Jude|1968|4|
|5|Smells Like Teen Spirit|1991|5|
|6|I Want To hold Your Hand|1963|4|

Il crée la table `interpretes` qui contient les interprètes et leur pays d'origine :

|id_interprete|nom|pays|
|:---:|:---:|:---:|
|1|Bob Dylan|États-Unis|
|2|Aretha Franklin|États-Unis|
|3|John Lennon|Angleterre|
|4|The Beatles|Angleterre|
|5|Nirvana|États-Unis|

`id_morceau` de la table `morceaux` et `id_interprete` de la table `interpretes` sont des clés primaires.

L’attribut `id_interprete` de la table `morceaux` fait directement référence à la clé primaire de la table `interpretes`.

# PARTIE 1 :
__1.a. Écrire__ le résultat de la requête suivante :
    ```sql
    SELECT titre
    FROM morceaux
    WHERE id_interprete = 4;
    ```
??? done "Solution"

    Il faut chercher! :smile:
        
    <!--__On obtient les titres 'Hey Jude' et 'I Want To hold Your Hand'.__-->

__1.b. Ecrire__ une requête permettant d'afficher les noms des interprètes originaires d'Angleterre.

??? done "Solution"

    Il faut chercher! :smile:
    
<!--```sql
    SELECT nom
    FROM interpretes
    WHERE pays = 'Angleterre'
    ```
-->

__1.c. Écrire__ le résultat de la requête suivante :
    ```sql
    SELECT titre, annee
    FROM morceaux
    ORDER BY annee;
    ```
??? done "Solution"

    Il faut chercher! :smile:
<!--       
    |||
    |:---:|:---:|
    |I Want To hold Your Hand|1963|
    |Like a Rolling Stone|1965|
    |Respect|1967|2|
    |Hey Jude|1968|
    |Imagine|1970|
    |Smells Like Teen Spirit|1991|

-->

__1.d. Écrire__ une requête permettant de calculer le nombre de morceaux dans latable `morceaux`.le résultat de la requête suivante :        

??? done "Solution"

    Il faut chercher! :smile:
    
<!--```sql
    SELECT COUNT(*)
    FROM morceaux
    ```
-->

__1.e. Écrire__ une requête affichant les titres des morceaux par ordre alphabétique.        

??? done "Solution"

    Il faut chercher! :smile:
    
<!--```sql
    SELECT titre
    FROM morceaux
    ORDER BY titre
    ```
-->

# PARTIE 2 :
__2.a. Citer__, en justifiant, la clé étrangère de la table `morceaux`. `

??? done "Solution"

    Il faut chercher! :smile:

    <!--__La clé étrangère est `id_interprete` qui fait référence à un attribut de la table `interpretes`.__-->

__2.b. Écrire__  un schéma relationnel des tables interpretes et morceaux.

??? done "Solution"

    Il faut chercher! :smile:

<!--
    - __morceaux(^^id_morceau^^, titre, annee, #id_interprete)__
    - __interpretes(^^id_interprete^^, nom, pays)__

    Les clés primaires sont indiquées soulignées(id_morceau et id_interprete).
    
    Dans la table morceaux, l’attribut id_interprete est précédé d’un # : c’est une clé étrangère faisant référence à l’attribut id_interprete de la table interpretes.
-->

__2.c. Expliquer__ pourquoi la requête suivante produit une erreur :

```sql
INSERT INTO interpretes
VALUES (1, 'Trust', 'France');
```

??? done "Solution"

    Il faut chercher! :smile:

    <!--__La table contient déjà une entrée dont l’attribut id_interprete vaut 1. Comme il s’agit de la clé primaire cela provoque une erreur.__-->


# PARTIE 3 :
__3.a.__ Une erreur de saisie a été faite. __Écrire__ une requête SQL permettant de changer l’année du titre « Imagine » en 1971.

??? done "Solution"

    Il faut chercher! :smile:
    
<!--```sql
    UPDATE morceaux
    SET annee = 1971
    WHERE id_morceau = 3
    ```
    Si l’on considère que les tables fournies représentent l’ensemble des données (le sujet est ambigu à ce titre), on peut aussi se contenter de :

    ```sql
    UPDATE morceaux
    SET annee = 1971
    WHERE titre = 'Imagine'
    ```
-->  

__3.b. Écrire__ une requête SQL permettant d'ajouter l'interprète « The Who » venant d'Angleterre à la table `interpretes`. On lui donnera un `id_interprete` égal à 6.

??? done "Solution"

    Il faut chercher! :smile:
    
<!--```sql
    INSERT INTO interpretes
    VALUES (6, 'The Who', 'Angleterre')
    ```
-->  

__3.c. Écrire__ une requête SQL permettant d'ajouter le titre « My Generation » de « The Who » à la table `morceaux`. Ce titre est sorti en 1965 et on lui donnera
un `id_morceau` de 7 ainsi que l'`id_interprete` qui conviendra.


??? done "Solution"

    Il faut chercher! :smile:
    
<!--```sql
    INSERT INTO morceaux
    VALUES (7, 'My Generation', 1965, 6)
    ```
-->  

# PARTIE 4 :
__4. Ecrire__ une requête permettant de lister les titres des interprètes venant des États-Unis.

??? done "Solution"

    Il faut chercher! :smile:
    
<!--```sql
    SELECT titre
    FROM morceaux
    JOIN interpretes ON interpretes.id_interprete = morceaux.id_interprete
    WHERE interpretes.pays = 'États-Unis'
-->  

[ ⬅️ Retour](../Bases_De_Donnees){ .md-button }
[📄 Accueil](../../../){ .md-button }