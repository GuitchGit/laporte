
[ ⬅️ Retour](../Bases_De_Donnees){ .md-button }

??? video
    

      
    
    |__Histoire des bases de données__|__Lumni : Qu’est-ce qu’une base de données relationnelle ?__|
    |:---:|:---:|
    |<iframe width="400" height="256" src="https://www.youtube.com/embed/iu8z5QtDQhY" title="L'histoire des bases de données" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>|<iframe width="400" height="256" src="//embedftv-a.akamaihd.net/a14ebeafdaf349fd0c7eec8b5570b806" frameborder="0" scrolling="no" allowfullscreen></iframe> |
    |__Interrogation d’une base de données relationnelle__||
    |<iframe width="400" height="256" src="//embedftv-a.akamaihd.net/5fafeb5be758d8c3e9f7a5e7d320a5f2" frameborder="0" scrolling="no" allowfullscreen></iframe>||
    
    
    __Capsule VIDEO : Madame JOIRON Céline__

    |__Caractérisation d'une base de données__|__Le modèle relationnel__|
    |:---:|:---:|
    |<iframe width="400" height="226" src="https://www.youtube.com/embed/alWIFoH5yEM" title="Sequence 0 - Introduction aux Bases de Données et SGBD" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>|<iframe width="400" height="226" src="https://www.youtube.com/embed/EnmsYuvXpwk" title="Sequence1: Le ModeleRelationnel" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>|
    |__SQL Manipulation de Données : Requêtes Mono-tables__|__SQL Manipulation de Données : Requêtes Multi-tables__|
    |<iframe width="400" height="226" src="https://www.youtube.com/embed/EnmsYuvXpwk" title="Sequence1: Le ModeleRelationnel" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>|<iframe width="400" height="226" src="https://www.youtube.com/embed/MoOoJnYCpoY" title="Sequence 4 : SQL Manipulation de Données - Requêtes multi-tables" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>|
           
        
 ![](img/Base_Donnees.png){ align=right}   

## __1. SGBD : Système de Gestion de Bases de Données__

Le SGBD peut être vu comme le logiciel qui gère les bases de données. Il permet de :
        
- __Décrire, Modifier la structure de la Base de données,__
- __Mémoriser__
- __Interroger__
- __Traiter et maintenir les données__
        
C’est l’interface entre les données et les utilisateurs.
        
Des systèmes de gestion de bases de données (SGBD) sont au centre de nombreux dispositifs de collecte, de stockage et de production d’informations.
        
!!! info "_`Exemples d'utilisation de la gestion de bases de données` :_"
    - Un hôpital (Patients, Docteurs, Hospitalisation, Chambres, Nombre de lits,...) ;
    - Un site de e-commerce (Articles, Commandes, Clients,...) ;
    - SNCF (Clients, réservations, trains,... )
    - ...
        
![](img/sql.png){width=15% align=right}
    


L’accès aux données d’une base de données relationnelle s’effectue grâce à des requêtes d’interrogation et de mise à jour qui peuvent être rédigées dans le langage __SQL (Structured Query Language)__.

## __2. Le modèle relationnel__

Le modèle relationnel est une manière de modéliser les informations contenues dans une base de données qui repose sur des principes mathématiques inventés en 1970 par [Edgar Franck Codd.](https://fr.wikipedia.org/wiki/Edgar_Frank_Codd){:target="_blank" }
        
<figure>
    <img src= ../img/Edgard_J_Codd.png width="50%">
</figure>
    
!!! note "Les pincipes de base du modèles relationnel"
    - __séparer__ les données dans plusieurs tables
        - chaque table contient des données relatives à un même sujet
        - éviter la redondance des données
        - ne pas stocker des données qui peuvent être calculées (exemple : une ligne Total)
        - chaque champ ne contient qu'une seule information
    - __mettre__ les tables en relation par l'utilisation de __clés__
                
# `Première relation`
    
Prenons l'exemple d'une base de données nécessaire à la gestion des formations dans une université.
        
La base de donnée possède une relation (une table) «ETUDIANT» :
        
<figure>
    <img src= ../img/relation_etudiant.png>
</figure>

!!! tldr "Vocabulaire"
    - __relation ou table__ : c'est l'endroit où sont rangées les données. L'ordre des lignes (que l'on appelera des enregistrements) n'a pas d'importance.

    - __enregistrement ou tuple ou n-uplet ou t-uplet ou vecteur__ : cela correspond à une ligne du tableau, et donc un ensemble de valeurs liées entre elles : L'étudiant `Dupont Jean` habite `Paris` et son numéro de téléphone est `0140404040`.
            
    Il est interdit que deux enregistrements soient totalement identiques (Ici, ils sont uniques grâce à l'attribut "NumETUD").
           
    ^^Remarque^^: Le nombre d'enregistrements d'une relation s'appelle son __cardinal__. Dans cette exemple, on a une cardinalité de 3.
            
    - __attribut__ : c'est l'équivalent d'une colonne. Il y a dans notre relation un attribut «NumETUD», un attribut «NomEtud», etc.
            
    - __domaine__ : le domaine désigne «le type» (au sens type Int, Float, String). L'attribut «NumETUD» est un entier (Int), par contre l'attribut «NomEtud» est une cha&Icircne de caractère (String).

    - __schéma__ : le schéma d'une relation est le regroupement de tous les attributs et de leur domaine respectif.
            
    Il existe aussi une autre manière de décrire le schéma relationnel :
            
    __ETUDIANT(^^NumETUD^^, NomEtud, PrenEtud, Adresse, Tel)__
            
# `Clé primaire` :key:

!!! note "Définition :heart:"
    Une clé primaire est un attribut (ou une réunion d'attributs) dont la connaissance suffit à identifier avec certitude un __unique enregistrement.__
            
    Par exemple, la clé primaire de la relation des personnes nées en France pourrait être leur numéro de Sécurité Sociale.
           
    Observons, dans notre relation précédente, ce qui peut être une clé primaire et ce qui ne peut pas l'être.

    - __NumETUD__ : cet attribut pourrait jouer le rôle de clé primaire. En effet, notre table ne contient pas deux étudiants ayant le même numéro.

    - __NomEtud__ : cet attribut ne peut pas jouer le rôle de clé primaire. En effet, la donnée de l'attribut «Dupont» renvoie à 2 étudiantss différents.

    - __PrenEtud__ : cet attribut pourrait jouer le rôle de clé primaire. En effet, notre table ne contient pas deux étudiants ayant le même prénom.

    - __Adresse__ : cet attribut pourrait jouer le rôle de clé primaire. En effet, notre table ne contient pas deux étudiants habitant la même ville.

    - __Tel__ : cet attribut pourrait jouer le rôle de clé primaire. En effet, notre table ne contient pas deux numéro de téléphone identique.

    Alors, quelle clé primaire choisir ? Il faut pour cela réfléchir à ce que deviendrait notre relation si elle contenait 1000 étudiants voire plus.
        
    Il est fort probable que deux étudiants habitent la même ville : l'attribut «Adresse» ne peut plus être une clé primaire.
    Il est fort probable que deux étudiants portent le même prénom : l'attribut «PrenEtud» ne peut plus être une clé primaire.
    On pourrait choisir l'attribut "Tel" puisque chaque étudiant doit avoir un numéro de téléphone différent mais cela peut poser des problème si l'étudiant change de numéro de téléphone.

    1. :warning: Attention, il ne peut pas y avoir deux clés primaires dans une table ;

    2. :warning: La clé primaire est toujours soulignée dans la relation ;

    3. :warning: La clé primaire peut être une combinaison(un tuple) de plusieurs attributs.

            
    La clé primaire choisie ici serait sans aucun doute l'attribut __«NumEtud»__.

# `D'autres relations`

Imaginons que l'on veuille ajouter le diplôme préparé par l'étudiant. La relation pourrait devenir :

<figure>
    <img src= ../img/relation_etudiant_diplome.png>
</figure>

On peut imaginer que beaucoup d'étudiant préparent une "Licence d'informatique".
        
De plus, dans cette licence, on pourrait avoir des spécialités. On crée une nouvelle relation "Diplômes" et on lie les deux relations par une clé étrangère.
On allège ainsi la base de données en retirant toutes les répétitions. 
        
<figure>
    <img src= ../img/relation_etudiant_diplome1.png>
</figure>

L'attribut «CodeDiplome» est une clé primaire de la relation «Diplômes».
        
# `Clé étrangère` :key:

Qu'est-ce qu'une clé étrangère en informatique ?
        
La clé étrangère est un outil essentiel dans une base de données (BDD) relationnelle. Elle permet de mettre en relation les différentes tables de la BDD. C'est aussi une contrainte qui assure __l'intégrité référentielle__ de celle-ci. Concrètement, la clé étrangère oblige une table à être liée aux données d’une autre table. 

!!! info "Définition :heart:"
    Une clé étrangère fait référence à la clé primaire d’une autre relation.

    :warning: Par convention, les clés étrangères sont représentées préfixées du symbole dièse #

# `Un autre exemple` :
        
Le diagramme suivant illustre un schéma de base de données de traitement des commandes

![](img/Exemple_cle_etrangere_produits.svg){width=100% align=center}
PK = Primary Key (Clé primaire)

FK = Foreign Key (Clé étrangère)
        
Le schéma présenté comporte trois tables :
        
- La table `Customers` enregistre les noms de chaque client;
- Les tables `Orders` permettent d'effectuer le suivi de toutes les commandes passées;
- La table `Products` stocke les informations sur chaque produit.`
        
Il existe deux relations de clé étrangère entre ces tables :
        
Une relation clé étrangère est définie entre la table `Orders` et la table `Customers` nommée __FK_CustomerOrder__ pour garantir qu'une commande ne puisse pas être créée, à moins qu'il n'y ait un client correspondant.
        
Une relation de clé étrangère nommée __FK_ProductOrder__ entre les tables `Orders` et `Products` garantit qu'il n'est pas possible de créer une commande pour un produit qui n'existe pas.

Exemple : Certains clients ont commandé des produits, comme indiqué dans la table des commandes. Grâce aux clés étrangères en place, nous pouvons garantir que les données insérées dans la table Orders ont une __intégrité référentielle.__

![](img/Exemple_cle_etrangere_produits_commande.svg){width=100% align=center}

Voyons ce qui se passe lorsque nous essayons de modifier les données afin de perturber l'intégrité référentielle.
        
__Ajouter une ligne dans la table Orders avec une valeur CustomerID qui n'existe pas dans Customers__
        
Que se passe-t-il si nous essayons la modification suivante, compte tenu des exemples de données du diagramme précédente ?
        
```sql
INSERT INTO Orders (OrderID, ProductID, Quantity, CustomerID)
VALUES (19, 337876, 4, 447);
```

Nous essayons d'insérer une ligne dans la table Orders avec un CustomerID (447) qui n'existe pas dans la table Customers. Autoriser cette opération générerait une commande non valide dans notre système. En revanche, la contrainte de clé étrangère que nous avons définie de la table Orders à la table Customers agit comme une protection. L'élément INSERT échoue avec le message suivant, en supposant que la contrainte est appelée FK_CustomerOrder.

`__Foreign key constraint `FK_CustomerOrder` is violated on table `Orders`. Cannot find referenced values in Customers(CustomerID).__`

__Tentative de suppression d'une ligne de la table Customers lorsque le client est référencé dans une contrainte de clé étrangère.__

Imaginons une situation dans laquelle un client résilie son compte sur la boutique en ligne. Nous souhaitons supprimer le client de notre base de données.

```sql
DELETE FROM Customers
WHERE CustomerID = 721;
```
Dans cet exemple, on détecte (via la contrainte de clé étrangère) qu'il existe encore des enregistrements dans la table Orders faisant référence à la ligne client que nous essayons de supprimer. Dans ce cas, l'erreur suivante s'affiche.

`__Foreign key constraint violation when deleting or updating referenced row(s): referencing row(s) found in table `Orders`.__`

Pour résoudre ce problème, on supprime toutes les entrées référençant le client dans Orders.

Vous voulez en savoir plus : [ici](https://www.data-bird.co/sql/cle-etrangere){:target="_blank" }
            
# __Les contraintes d'intégrité__

Pour qu'un SGBD fonctionne correctement, il faut que la base de données respecte des contraintes d'intégrité. Il faut que l'ensemble des données soient cohérentes. Les contraintes d'intégrité doivent être vérifiées à chaque instant d'utilisation de la base de données, en particulier lors de la modification de celle-ci.

!!! note "__Les contraintes d'intégrité__"
    - __Contrainte de domaine__ : Cela correspond au type donné dans le schéma de la relation. Cette contrainte est fixée par le schéma de la relation et dépend du traitement envisagé.
    - __Contrainte de relation (ou d'entité)__ : Elle permet de s'assurer que chaque entité d'une relation est unique, sans ambiguïté. À cette fin, on utilisera la notion de clé primaire qui sera l'identifiant unique d'une entité dans une relation.
    - __Contrainte de référence__ : Dans une base de données comportant plusieurs relations, il est fréquent qu'une table fasse référence à une entité d'une autre. Pour cela on utilise des clé étrangères. Il faut donc s'assurer à tout instant, que chaque clé étrangère mentionnée dans une entité corresponde à une unique entité qui existe encore.
    - __Contrainte de l'utilisateur__ : Il peut-être utile de préciser certaines conditions sur le domaine d'un attribut.

!!! done "Exemple"
    - Un code postal qui est, à la base, un nombre entier pourra avoir un domaine de type cha&Icircne de caractères afin d'obliger le nombre de 5 caractères y compris pour les codes tels que : "01100".
    La contrainte de domaine sera donc une chaîne de caractères (Dans certains SGBD, on pourra quantifier la longueur, ici 5)

    - Dans un répertoire téléphonique, certaines personnes peuvent avoir le même nom et éventuellement aussi le même prénom. Donc ni le nom, ni le prénom ne peuvent être choisis comme clé primaire. Par contre à un numéro de téléphone, ne correspond qu'une seule personne. Celui-ci peut donc être considéré comme clé primaire.

    - Dans une base de données de gestion de commandes des clients. On considère trois tables : Client, Produit et Commande. Chaque entité de la table commande contiendra des références à la table Client et à la table Produit. Afin d'avoir une cohérence de la base de données, il faut toujours s'assurer que dans la table Commande chaque référence à un client correspond bien à un client existant. Ce qui implique :
        - De vérifier l'existence d'un client lors d'une nouvelle commande
        - De ne pouvoir supprimer un client de la table client, sans s'assurer qu'il n'existe pas de commande le référençant. On se retrouverai avec une commande d'un client qui n'existe pas.
    - On ne peut pas modifier la clé primaire correspondant à un client si il est référencé dans la table commande.
            
    - Dans une base de données où on utilise l'âge, il peut être utile de vérifier que l'âge entré est positif.


[ ⬅️ Retour](../Bases_De_Donnees){ .md-button }
[📄 Accueil](../../../){ .md-button }
