[ ⬅️ Retour](../Bases_De_Donnees){ .md-button }

# __`Oh mon beau QCM!`__

![](img/Exercices/QCM.svg){width=15% align=left}
Un enseignant a mis en place un site web qui permet à ses élèves de faire des QCM (questionnaire à choix multiples) de NSI en ligne.

L’enseignant a créé une base de données nommée __QCM_NSI__ pour gérer ses QCM, contenant les quatre relations (appelé aussi communément "table") du schéma relationnel ci-dessous :

![](img/Exercices/Schema_Exercice_1.png){width=75%}

Dans le schéma relationnel précédent, un attribut souligné indique qu’il s’agit d’une __clé primaire__.

Un attribut précédé du symbole # indique qu’il s’agit d’une __clé étrangère__ et la flèche associée indique l’attribut référencé.

Ainsi, par exemple, l'attribut `ideleve` de la relation `lien_eleve_qcm` est une clé étrangère qui fait référence à l'attribut `ideleve` de la relation `eleves`.

Dans le cas de la relation `lien_eleve_qcm` la clé primaire est composée de l’association des deux attributs `ideleve` et `idqcm`, eux-mêmes étant des clés
étrangères.

On donne ci-dessous le contenu exhaustif des relations :
![](img/Exercices/Tables_Exercice_1.png){width=75%}

# PARTIE 1 :

__1.a.__ Que retourne la requête suivante ?
    ```sql
    SELECT titre
    FROM `qcm`
    WHERE date>'2022-01-10'
    ```
??? done "Solution"

    Il faut chercher! :smile:
        
<!--__Affiche les titres des QCM créés antérieurement au 10 janvier 2022.__

    - __POO__
    - __Arbre Parcours__
-->

__1.b. Ecrire__ une requête qui donne les notes de l’élève qui a pour identifiant 4.

??? done "Solution"

    Il faut chercher! :smile:
    

<!--```sql
    SELECT note
    FROM lien_eleve_qcm
    WHERE ideleve = 4
    ORDER BY note DESC
    ```
-->

# PARTIE 2 :

__2.a.__ Sachant que la clé primaire de la relation lien_eleve_qcm est composée de l’association des deux attributs `ideleve` et `idqcm`, __expliquer__ pourquoi avec ce
schéma relationnel, un élève ne peut pas faire deux fois le même QCM.

??? done "Solution"

    Il faut chercher! :smile:

<!--__La clef primaire doit être unique__
-->

__2.b.__ L’élève Marty Mael vient de faire le QCM sur la POO et a obtenu une note de 18.
Comment est/sont modifiée(s) le(s) table(s) ? Il n’est pas demandé d’écrire une requête SQL.

??? done "Solution"

    Il faut chercher! :smile:

<!--
    - Lors de la 1ère question, un enregistrement est créé dans la table lien_eleve_qcm (INSERT).
    - Puis à chaque réponse de l’élève la valeur de l’attribut note est modifiée (UPDATE).
-->

__2.c.__ Un nouvel élève (nom : Lefèvre, prenom : Kevin) est enregistré. __Ecrire__ la requête permettant la mise à jour du/des relation(s).

??? done "Solution"

    Il faut chercher! :smile:

<!--```sql
    INSERT INTO eleves
    VALUES(6, "Lefèvre", "Kevin")
    ```
-->

__2.d.__ L’élève Dubois Thomas quitte l’établissement et toutes les références à cet élève doivent être supprimées des relations. Pour la relation `lien_eleve_qcm`, __écrire__ la requête pour supprimer toutes les références à l’élève Dubois Thomas qui a pour identifiant 2.

??? done "Solution"

    Il faut chercher! :smile:

<!--```sql
    DELETE FROM lien_eleve_qcm WHERE ideleve = 2 ;
    DELETE FROM eleves WHERE ideleve = 2 ;INSERT INTO eleves
    ```
    __Toutes les références à cet élève doivent être supprimées__

-->

# PARTIE 3 :
__3.a. Recopier__ et compléter les ……………………………………… de la requête suivante pour qu’elle affiche la liste des noms et prénoms des élèves ayant fait le QCM d’`idqcm` égal 4.
    ```sql
    SELECT ……………………………………… FROM eleves
    JOIN lien_eleve_qcm ON eleves.ideleve = ……………………………
    WHERE ……………………………… ;
    ```

   
??? done "Solution"

    Il faut chercher! :smile:
    
<!--```sql
    SELECT noms, prenom FROM eleves
    JOIN lien_eleve_qcm ON eleves.ideleve = lien_eleve_qcm.ideleve
    WHERE idqcm = 4 ;
    ```
-->  

__3.b. Donner__  le résultat de la requête de la question a.

??? done "Solution"

    Il faut chercher! :smile:
    
<!--
    - Dubois Thomas
    - Marty Mael
    - Bikila Abebe
-->

# PARTIE 4 :
__4. Ecrire__ une requête qui affiche le nom, le prénom et la note des élèves ayant fait le QCM Arbre Binaire. L’utilisation des trois tables dans la requête est attendue.

??? done "Solution"

    Il faut chercher! :smile:
    
<!--```sql
    SELECT eleves.nom, eleves.prenom, lien_eleve_qcm.note
    FROM eleves, lien_eleve_qcm, qcm
    WHERE qcm.titre = "Arbre Binaire"
    AND lien_eleve_qcm.idqcm = qcm.idqcm AND eleves.ideleve = lien_eleve_qcm.ideleve
    ORDER BY eleves.nom, eleves.prenom

-->  
[ ⬅️ Retour](../Bases_De_Donnees){ .md-button }
[📄 Accueil](../../../){ .md-button }