

<!--Testez :

{!{ sqlide titre="Tape ta requête SQL ici :" init="BDD/les_Aliens0.sql"}!}
-->


=== "Cours"

    !!! note "Modèle relationnel : relation, attribut, domaine, clef primaire, clef étrangère, schéma relationnel[![](img/BO_Base_Donnees.png){ align=right}](../modele_relationnel)"
    
    !!! note "Langage SQL : requêtes d’interrogation et de mise à jour d’une base de données.[![](img/BO_Sql.png){ align=right}](../langage_SQL)"

=== "Exercices requêtes SQL"
    ??? note "Exercice 1 : Agences de locations de voitures"
    
        Le schéma relationnel

        ![Schema_Relationnel_Locations](../img/Schema_relationnel_locations.png)
        
        - **Commande :** 
            ```sql
            SELECT *
            FROM vehicules;
            ``` 
        - **Exécution:**

        __Visualiser__ toutes la relation `vehicules` :

        {!{sqlide titre="Tape ta requête SQL ici :" init="BDD1/locations.sql"}!}

    ####       
    ??? note "Exercice 2 : Les communes, départements et régions de France"
        Le schéma relationnel

        !A venir



        __Exécuter__ les 3 requêtes suivantes:
        === "Visualiser toutes les communes"
            ```sql
            SELECT * 
            FROM communes
            ```
        === "Visualiser tous les départements"
            ```sql
            SELECT * 
            FROM departements
            ```
          
        === "Visualiser toutes les régions"
            ```sql
            SELECT * 
            FROM regions
            ```

        {!{ sqlide titre="Tape ta requête SQL ici :" init="BDD1/france.sql"}!}
        
        __Q1. Quelles sont les communes du département 60 ?__

        {!{ sqlide titre="Tape ta requête SQL ici :" init="BDD1/france.sql"}!}
    
        __Q2. Ecrire une requêtes permettant de donner le nombre de communes du département 60 ?__
        
        {!{ sqlide titre="Tape ta requête SQL ici :" init="BDD1/france.sql"}!}
    
    ??? note "Exercice 3 : Gestion simplifier d'un lycée"
        On travaille avec la base de données dont le schéma est donné ci-dessous:

        ![](img/Base_Donnees_Eleves.png){: .center}



        __Exécuter__ les 2 requêtes exemples suivantes:
        === "Exemple 1"
            On veut afficher le nom du ou des professeurs enseignant la spécialité pour le groupe dont l'identifiant `grpid` est `1SVTG1`.

            Il s'agit d'un affichage, la requête commence par __SELECT__.

            Les noms des professeurs sont contenus dans la colonne `nom` de la table ^^Professeur^^.

            L'information sur qui enseigne quoi est dans la table ^^Enseigne^^. Cette table contient les `grpid`.

            On va donc devoir effectuer une __jointure__ entre les deux tables.
            
            Pour que le résultat associe un professeur aux groupes où il enseigne, on va utiliser comme critère de jointure l'égalité entre les `pid`, en écrivant soit `ON professeur.pid=Enseigne.pid`.

            De plus on ne veut que les noms du ou des professeurs enseignant du groupe `1SVTG1` : on va donc appliquer un filtre avec WHERE : `WHERE grpid="1SVTG1"`

            La requête complète s'écrira :
                ```sql
                SELECT nom FROM Professeur 
                JOIN Enseigne ON Professeur.pid=Enseigne.PID 
                WHERE grpid='1SVTG1'
                ```
               
        === "Exemple 2"
            On veut les noms des élèves suivant la spécialité d'intitulé "Physique-Chimie".
            Il s'agit d'un affichage, la requête commence par SELECT.

            Les noms des élèves sont dans la colonne nom de la table Lyceen.
            Les intitulés des spécialités sont dans la table Groupe.    
            L'information sur les inscriptions en spécialité est dans la table Affectation.

            On va donc devoir faire une jointure entre ces trois tables. La jointure entre Lyceen et Affectation va être faite en utilisant id, celle entre Affectation et Groupe en utilisant grpid.

            On veut seulement les noms des élèves inscrits en Physique-Chimie : on ajoute donc un filtre "WHERE".

            La requête peut s'écrire :
                ```sql
                SELECT nom FROM Lyceen
                JOIN Affectation ON Affectation.id = Lyceen.id
                JOIN Groupe ON Groupe.grpid=Affectation.grpid
                WHERE Intitule="Physique-Chimie"
                ```
        {!{ sqlide titre="Tape ta requête SQL ici :" init="BDD1/eleve.sql"}!}
    
        Exercice 1 : Jointures impliquant deux tables 

        a. __Utiliser__ une jointure entre les tables ^^Lyceen^^ et ^^Affectation^^ pour associer chaque lycéen inscrit dans un groupe de spécialité avec chacun des identitfiants `grpid` des groupes où il est inscrit, en affichant le résultat sous la forme

        ![](img/Base_Donnees_Eleves.png){: .center}

        | **nom** | **prenom** | **grpid** |
        |---|---|---|
        | Tilliou |	Sébastien |	TMathG2 |
        | Hébert |	Juliette |	TMathG2 |
        | Ouaderni | Lina |	TSVTG1 |
        | etc | ... |...  |

        {!{ sqlide titre="Tape ta requête SQL ici :" init="BDD1/eleve.sql"}!}

        b. __Utiliser__ une jointure entre les tables ^^Professeur^^ et ^^Enseigne^^ pour associer chaque nom de professeur avec chacun des identitfiants `grpid` des groupes de spécialité dans lesquels il enseigne. On doit obtenir une table de la forme

        ![](img/Base_Donnees_Eleves.png){: .center}

        | **nom** | **grpid**  | 
        |---|---|
        | Mr Bernal |	1NSIG1 |
        | Mme Lieville | TMathG2 |
        | Mr El Khatib | 1MathG2 |
        | Mr Scalaire |	1MathG3 |  

        comptant 27 lignes

        {!{ sqlide titre="Tape ta requête SQL ici :" init="BDD1/eleve.sql"}!}

    ??? note "Exercice 4 : SQL Cours et exercices"
    
        [Ici](https://fxjollois.github.io/cours-sql/){:target="_blank" }



=== "Exercices Type BAC" 
    !!! note "[Base de données : Gestion de QCM](../Exercice_Type_Bac1)"
        <!-- Sujet Mayotte 2022 - J1 - Ref : 22-NSIJ1LR1 -->

    !!! note "[Base de données : Le musicien](../Exercice_Type_Bac2)"
        <!-- Sujet métropole 2022 - J2 - Ref : 22-NSIJ2ME1 -->


=== "Ressources"   
    
    Sur Lumni :
        
    - [Qu’est-ce qu’une base de données relationnelle ?](https://www.lumni.fr/video/qu-est-ce-qu-une-base-de-donnees-relationnelle){target=__blank}
    - [Interrogation d’une base de données relationnelle](https://www.lumni.fr/video/interrogation-d-une-base-de-donnees-relationnelle){target=__blank}
    
    Pour la gestion des accents sur les fichiers SQL, clique [ICI](https://www.scriptol.fr/creation-site-web/accents-html.php){target=__blank}

    Pour réaliser des schémas relationnels, clique [ICI](https://dbdiagram.io/home){target=__blank} ou [ICI](https://www.looping-mcd.fr/){target=__blank}
 

    ???+ note "Différents moyens d'exploiter une base de données"
        
        On travaille avec la base de données dont le schéma est donné ci-dessous:
        
        **Pré-requis :** télécharger la base de données [livres.db](../data/livres.db){:target="_blank"}.

        ??? abstract "1. En ligne avec ```sqliteonline.com``` "
            - Sur [https://sqliteonline.com/](https://sqliteonline.com/){target=__blank}
            - Par __File__ / __OpenDB__, ouvrir le fichier  ```livres.db``` précédemment téléchargé.
            -    Écrire votre requête plus cliquer sur Run.  
                ![](../img/sqlonline.png)

        ??? abstract "2. Au sein d'un notebook Jupyter"
            - Si nécessaire, installer via le terminal les paquets suivants :
            ```python
            ! install jupyter-sql
            ! install ipython-sql
             ``` 
            - Dans un notebook Jupyter, votre première cellule doit être 
            ```python
            %load_ext sql
            
            %sql sqlite:///livres.db
            ``` 
            en ayant bien pris soin de mettre le fichier ```livres.db``` dans le même répertoire que votre fichier Jupyter.
            Ensuite, chaque requête devra être précédée de la ligne ```%% sql```.
            ![](../img/jupyter.png)

        ??? abstract "3. Avec un logiciel externe : DB Browser for SQLite :star: :star: :star:"
            - Installer ```DB Browser for SQLite```, téléchargeable à l'adresse [https://sqlitebrowser.org/](https://sqlitebrowser.org/)
            - Ouvrir le fichier ```livres.db```.  
            ![](../img/dbbrowser.png)





--------


[📄 Accueil](../../../){ .md-button }

