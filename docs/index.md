<!-- # Image flottantes
![Logo_Calvin1](img/Logo_Calvin1.png){width=40% align=right}-->

# La spécialité Numérique Sciences de l'Informatique
![Logo_Calvin1](img/Logo_Calvin1.png){width=25% align=center}
![](img/laptop.gif){width=20% align=center}

=== " 👨‍🎓 PREMIÈRE"
    !!! note ""
        1. Histoire de l’informatique --> A venir 
        2. [Représentation des données : Types et valeur de base](../laporte/1NSI/ch02/ch02)
        3. Représentation des données : Types construits
        4. Traitement de données en tables
        5. Interactions entre l’homme et la machine sur le Web
        6. [Architectures matérielles et systèmes d’exploitation](../laporte/1NSI/ch06/ch06)
        7. [Langages et programmation](../laporte/1NSI/ch07/ch07)
        8. [Algorithmiques](../laporte/1NSI/Algorithmique/Tri/Tri) 
        9. [Projet](../laporte/1NSI/Projet/Pygame/Pygame)



=== " 👨‍🎓👨‍🎓 TERMINALE"
    !!! note ""
        1. [Révisions](../laporte/TNSI/Revisions/Revisions)
        2. [Bases de données - Langage SQL](../laporte/TNSI/Bases_De_Donnees/Bases_De_Donnees)
        2. [Récursivité](../laporte/TNSI/Recursivite/Recursivite)
        3. La programmation orientée objet
        4. Les arbres
        5. Les piles
        6. Les files
        7. Les graphes
        8. Les processus
        9. Le routage : RIP et OSFP
        10. Diviser pour régner
        11. [Épreuves pratiques](../laporte/TNSI/EP/EP)

=== " 🐍 PYTHON"
    ??? info "Ressources"
        ## `Editeur Python`
            
        - Un Environnement de développement pour l'apprentissage de Python. Peut être installé sur un disque dur ou une clé USB sans droits administrateur :  [^^edupyter^^](https://www.edupyter.net/){:target="_blank"}
        - Une distribution complète avec un interface simple et un débogueur très visuel : [^^Thonny^^](https://thonny.org/){:target="_blank"}
        - Une distribution plus lourde mais plus complète avec tous les modules scientifiques : [^^Anaconda^^](https://www.anaconda.com/products/distribution){:target="_blank"}
        - Un site pour tester des programmes Python ou des Notebooks Jupyter en ligne :
            - [^^La console Basthon^^](https://console.basthon.fr/){:target="_blank"}
            - [^^Notebook Basthon^^](https://notebook.basthon.fr/){:target="_blank"}
        - __Tester_ et __visualiser__ un programme pas à pas : [^^Pythontutor^^](https://pythontutor.com/visualize.html#mode=edit){:target="_blank"} 
        
        ## `Exemple de code Python`
        - [^^Snippets^^](http://lycee.educinfo.org/python-snippets/){:target="_blank"}

        ## `Linux en ligne`
        - [^^WebLinux^^](https://moocbash.univ-reunion.fr/?cpu=asm&n=1){:target="_blank"}  

    
    !!! note "Niveau Débutant"
        - [^^e-nsi^^](https://e-nsi.gitlab.io/pratique/tags/#0-simple){:target="_blank"}
        - [^^Futurtecoder^^](https://fr.futurecoder.io/course/#toc){:target="_blank" }
        - [^^Caseine^^](https://moodle.caseine.org/course/view.php?id=87){:target="_blank" }

    !!! note "Niveau Intermédiaire"
        - [^^France IOI^^](http://www.france-ioi.org/){:target="_blank" }
        - En anglais : [^^hackinscience^^](https://www.hackinscience.org/exercises/){:target="_blank" }
        

    !!! note "Niveau Professionnel lol"


    !!! note "Je teste d'autres langages"
        - [^^glot.io^^](https://glot.io/new){:target="_blank" }


