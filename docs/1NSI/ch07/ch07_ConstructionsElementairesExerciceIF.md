[ ⬅️ Retour](../ch07){ .md-button }
[📄 Accueil](../../../){ .md-button }

!!! warning "ATTENTION"
    Pour chaque exercice demandé, si vous devez utiliser un IDE,  avant de commencer il faut appuyer sur l'icône :  ![](../img/rafraichir.png)

??? note "__`Exercice N°1`__ : Structure IF / ELIF / ELSE "

    ## __`Enoncé`__ :

    __Lancer__ le programme dans l'IDE ci-dessous et __observer__ le résultat . Pour analyser l'influence de la valeur de x, vous pouvez modifier judicieusement cette valeur afin d'observer l'ensemble des résultats possibles à obtenir.
            
    ```python
    x = 25
    if x < 4:
        print("petit")
    elif x < 20:
        print("moyen")
    else:
        print("grand")
    ```

    {{ IDE() }}
    
    
??? note "__`Exercice N°2`__ : Positif ou négatif?"

    Soit le programme suivant où l'on peut changer la valeur de a par un entier positif ou négatif.
    
    ```python linenums="1"
    a = 12 # a doit être judicieusement choisi afin de tester votre programme 
    # A compléter
    
    ```
    
    __Compléter__ ce programme afin qu'il affiche si le nombre affecter _`a`_ est positif ou négatif (Voir les exemples ci-dessous). __Réaliser__ plusieurs essais afin de valider votre proposition.
    
    - ^^Exemple1^^ : si _`a = 12`_ -> Le programme retourne a est positif
    - ^^Exemple2^^ : si _`a = -12`_ -> Le programme retourne a est négatif


    {{ IDE() }}

    ??? done "Solution"
            
        Il faut chercher! :smile:
                    

    <!--```python linenums="1"
        a = -12 # a doit être jusdicieusement choisi afin de tester votre programme
        # A compléter
        if a>=0:
            print('a est positif')
        else:
            print('a est negatif')
    -->

??? note "__`Exercice N°3`__ : Calcul du tarif d'une lettre "

    ## __`Enoncé`__ :

    En utilisant le lien suivant : [ICI](https://www.prixdutimbre.fr/tarifs-postaux-affranchissement-la-poste/){:target=__blank}
    
    __Ecrire__ un programme qui affiche le prix de l'affranchissement d'une lettre en fonction de son type et de son poids (le physicien aurait préféré masse).
        
    {{ IDE() }}


<!--
??? note "__`Exercice N°4`__ "

    ## __`Enoncé`__ :

    - Choisir un nombre ;
    - Le multiplier par 2 ;
    - Ajouter 5 ;
    - Multiplier le résultat par le nombre de départ ;
    - Afficher le résultat.


    __1.a. Calculer__ le résultat pour __x=-1__ et __x=10__

    ??? done "Solution"
            
        Il faut chercher! :smile:
                
        <!-- si __x=-1__ le résultat vaut : __-3__ , si x=10 le résultat vaut : __250__

    __1.b.__ Implémenter l'algorithme sur Python

    Vous pouvez faire enterer une valeur à l'utilsateur avec la fonction input()
    ```python linenums="1"
    x =int(input("Entrer une valeur pour x: "))
    ```
    Il est toutefois préférable de simplement affecter la valeur à la main dans votre code. Une solution avec l'utilisation d'une fonction sera demandé dans un prochain chapitre.

    {{ IDE() }}

    ??? done "Solution"
            
        Il faut chercher! :smile:
                    

    <!--   
        ```python linenums="1"
        x = -1 # Remplacer x par la valeur souhaitée
        y = 2*x
        y = y + 5
        x = y*x
        print(x)
        ``` 
    

??? note "__`Exercice N°5`__ "
        
    ## __`Enoncé`__ :

     __Écrire__ le code pour:

    - Initialiser une variable score à 10
    - Augmenter de 15 ce score

    {{ IDEv() }}


    ??? done "Solution"
            
        Il faut chercher! :smile:
                
        <!--```python linenums="1"
        score = 10
        score = score + 15
        ```

        ou:

        <!--```python linenums="1"
        score = 10
        score += 15
        ```
        --
    

??? note "__`Exercice N°6`__ : Le prix c'est le prix! "
        
    ## __`Enoncé`__ :

    On souhaite effectuer une remise sur un prix lors de l'achat d'un vêtement qui coûte 35€. Pour cela, votre programme devra afficher le prix de cet article et le prix remisé de 20%.

    __1.a. Proposer__ une liste de variables nécessaires pour ce programme ;
            
    __1.b. Préciser__ le type de vos variables utilisées ;
            
    __1.c. Proposer__ un algorithme pour résoudre cet énoncé et réaliser le programme sur Python dans l'IDE suivant.
        {{ IDE() }}

    ??? done "Solution"
            
        Il faut chercher! :smile:
                
        <!--```python linenums="1"
        prix = 35
        prix_remise = prix * 0.8 # remise de 20 %
        print(prix) # ne sera pas affiché
        print(prix_remise) # sera évalué et renvoyé car en dernière ligne 
        ```
        --

??? note "__`Exercice N°7`__ "

    ## __`Enoncé`__ :

    En utilisant une seule variable auxiliaire (aux) et sans utiliser de tuples, effectuer la permutation circulaire des variables des x, y, z, t selon le schéma :

    ![](img/permutation.png){width=25% }

    Donc si : __x = 10, y = 11, z = 12 et t = 13__

    Après la permutation, on doit obtenir : __x = 13, y = 10, z = 11 et t = 12__
            {{ IDEv() }}

    ??? done "Solution"
            
        Il faut chercher! :smile:
                
        <!--```python linenums="1"
            x=10
            y=11
            z=12
            t=13
            tmp=t
            t=z
            z=y
            y=x
            x=tmp
            ```
        --

??? note "__`Exercice N°8 : Script avec erreur ou sans?`__ "

    <iframe src="https://learningapps.org/watch?app=13710073" style="border:0px;width:100%;height:400px" webkitallowfullscreen="true" mozallowfullscreen="true"></iframe>.


??? note "__`Exercice N°9 : Dessiner avec le module Turtle de Python`__ "

    Le module turtle est un outil du langage Python de tracé de figures simples, il a l'avantage de pouvoir être utilisé avec très peu de connaissances.

    Un curseur (la tortue) effectue le tracé à l'écran en se déplaçant selon les instructions codées par l'utilisateur.
    
    L'entête de votre code devra comporter l'importation de ce module: __`from turtle import *`__. (Déjà saisi dans l'IDE ci-dessous)

    |Commande|Fonction||Commande|Fonction|
    |:---:|:---:|:---:||:---:|
    |__forward(N)__|avance le curseur de N pixels||__down()__|descend le crayon|
    |__backward(N)__|recule le curseur de N pixels||__color(couleur)__|colorie le tracé|
    |__left(a)__|tourne le curseur de a° vers la gauche||__begin_fill()__|active le mode remplissage|
    |__right(a)__|tourne le curseur de a° vers la droite||__end_fill()__|désactive le mode remplissage|
    |__goto(x,y)__|déplace le curseur au point de coordonnées (x,y)||__fillcolor(couleur)__|sélectionne la couleur de remplissage|
    |__up()__|monte le crayon: le tracé ne s'effectue plus|||
    
    __1. Dessiner__ un carré de 50 par 50 pixels, puis deux carrés adjacents de 80 par 80 pixels;

    __2. Dessiner__ un hexagone.
    {{ IDE('turtle') }}

    ??? done "Solution"
            
        Il faut chercher! :smile:

    <!--```python linenums="1"
        # Hexagone
        from turtle import *
        # Hexagone
        left(30)
        forward(50)
        right(60)
        forward(50)
        right(60)
        forward(50)
        right(60)
        forward(50)
        right(60)
        forward(50)
        right(60)
        forward(50)
        ```
        -->

[ ⬅️ Retour](../ch07){ .md-button }
[📄 Accueil](../../../){ .md-button }