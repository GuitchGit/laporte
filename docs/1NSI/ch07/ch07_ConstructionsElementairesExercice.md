[ ⬅️ Retour](../ch07){ .md-button }
[📄 Accueil](../../../){ .md-button }

!!! warning "ATTENTION"
    Pour chaque exercice demandé, si vous devez utiliser un IDE,  avant de commencer il faut appuyer sur l'icône :  ![](../img/rafraichir.png)

??? note "__`Exercice N°1`__ : Les affectations "

    ## __`Enoncé`__ :

    Ci-dessous, __déplacer__ les affectations afin de les classer soit dans la partie "Non valide" ou "valide"
            
    <iframe src="https://learningapps.org/watch?v=pkyk2o9za20" style="border:0px;width:100%;height:400px" webkitallowfullscreen="true" mozallowfullscreen="true"></iframe>



??? note "__`Exercice N°2`__ : Les variables "

    ## __`Enoncé`__ :

    __Écrire__ le programme Python correspondant à l'algorithme ci_dessous. __Vérifier__ également vos réponses avec l'IDE ci-dessous.
            
    Que valent N et P après l’exécution de cet algorithme ? __Afficher__ leur type.

    N ← 2
            
    P ← 3
            
    N ← P + 1
            
    P ← N
    {{ IDE() }}

??? note "__`Exercice N°3`__ : Permutations "

    Pour permuter les valeurs de deux variables, on essaie le code suivant :

    ```python linenums="1"
    >>> x = 1
    >>> y = 100
    >>> y = x
    >>> x = y
    >>> x
    1
    >>> y
    1 
    ```
    __1.a. Expliquer__ ce qu'il s'est passé:

    ??? done "Solution"
            
        Il faut chercher! :smile:
                
        <!-- y a été affecté à la valeur x soit 1 (ligne3) puis on affecte y à x (ligne 4), donc : x=y=1-->

    __1.b.__ Comment faire pour remédier à cette erreur ?

    ??? done "Solution"
            
        Il faut chercher! :smile:
                    
        <!--
        ```python linenums="1"
        >>> x = 1
        >>> y = 100
        >>> tmp = x
        >>> x = y
        >>> y = tmp
        >>> x
        100
        >>> y
        1 
        ```
        -->


??? note "__`Exercice N°4`__ "

    ## __`Enoncé`__ :

    - Choisir un nombre ;
    - Le multiplier par 2 ;
    - Ajouter 5 ;
    - Multiplier le résultat par le nombre de départ ;
    - Afficher le résultat.


    __1.a. Calculer__ le résultat pour __x=-1__ et __x=10__

    ??? done "Solution"
            
        Il faut chercher! :smile:
                
        <!-- si __x=-1__ le résultat vaut : __-3__ , si x=10 le résultat vaut : __250__-->

    __1.b.__ Implémenter l'algorithme sur Python

    Vous pouvez faire enterer une valeur à l'utilsateur avec la fonction input()
    ```python linenums="1"
    x =int(input("Entrer une valeur pour x: "))
    ```
    Il est toutefois préférable de simplement affecter la valeur à la main dans votre code. Une solution avec l'utilisation d'une fonction sera demandé dans un prochain chapitre.

    {{ IDE() }}

    ??? done "Solution"
            
        Il faut chercher! :smile:
                    

    <!--   
        ```python linenums="1"
        x = -1 # Remplacer x par la valeur souhaitée
        y = 2*x
        y = y + 5
        x = y*x
        print(x)
        ``` 
    -->

??? note "__`Exercice N°5`__ "
        
    ## __`Enoncé`__ :

     __Écrire__ le code pour:

    - Initialiser une variable score à 10
    - Augmenter de 15 ce score

    {{ IDEv() }}


    ??? done "Solution"
            
        Il faut chercher! :smile:
                
        <!--```python linenums="1"
        score = 10
        score = score + 15
        ```

        ou:

        <!--```python linenums="1"
        score = 10
        score += 15
        ```
        -->
    

??? note "__`Exercice N°6`__ : Le prix c'est le prix! "
        
    ## __`Enoncé`__ :

    On souhaite effectuer une remise sur un prix lors de l'achat d'un vêtement qui coûte 35€. Pour cela, votre programme devra afficher le prix de cet article et le prix remisé de 20%.

    __1.a. Proposer__ une liste de variables nécessaires pour ce programme ;
            
    __1.b. Préciser__ le type de vos variables utilisées ;
            
    __1.c. Proposer__ un algorithme pour résoudre cet énoncé et réaliser le programme sur Python dans l'IDE suivant.
        {{ IDE() }}

    ??? done "Solution"
            
        Il faut chercher! :smile:
                
        <!--```python linenums="1"
        prix = 35
        prix_remise = prix * 0.8 # remise de 20 %
        print(prix) # ne sera pas affiché
        print(prix_remise) # sera évalué et renvoyé car en dernière ligne 
        ```
        -->

??? note "__`Exercice N°7`__ "

    ## __`Enoncé`__ :

    En utilisant une seule variable auxiliaire (aux) et sans utiliser de tuples, effectuer la permutation circulaire des variables des x, y, z, t selon le schéma :

    ![](img/permutation.png){width=25% }

    Donc si : __x = 10, y = 11, z = 12 et t = 13__

    Après la permutation, on doit obtenir : __x = 13, y = 10, z = 11 et t = 12__
            {{ IDEv() }}

    ??? done "Solution"
            
        Il faut chercher! :smile:
                
        <!--```python linenums="1"
            x=10
            y=11
            z=12
            t=13
            tmp=t
            t=z
            z=y
            y=x
            x=tmp
            ```
        -->

??? note "__`Exercice N°8 : Script avec erreur ou sans?`__ "

    <iframe src="https://learningapps.org/watch?app=13710073" style="border:0px;width:100%;height:400px" webkitallowfullscreen="true" mozallowfullscreen="true"></iframe>.


??? note "__`Exercice N°9 : Dessiner avec le module Turtle de Python`__ "

    Le module turtle est un outil du langage Python de tracé de figures simples, il a l'avantage de pouvoir être utilisé avec très peu de connaissances.

    Un curseur (la tortue) effectue le tracé à l'écran en se déplaçant selon les instructions codées par l'utilisateur.
    
    L'entête de votre code devra comporter l'importation de ce module: __`from turtle import *`__. (Déjà saisi dans l'IDE ci-dessous)

    |Commande|Fonction||Commande|Fonction|
    |:---:|:---:|:---:||:---:|
    |__forward(N)__|avance le curseur de N pixels||__down()__|descend le crayon|
    |__backward(N)__|recule le curseur de N pixels||__color(couleur)__|colorie le tracé|
    |__left(a)__|tourne le curseur de a° vers la gauche||__begin_fill()__|active le mode remplissage|
    |__right(a)__|tourne le curseur de a° vers la droite||__end_fill()__|désactive le mode remplissage|
    |__goto(x,y)__|déplace le curseur au point de coordonnées (x,y)||__fillcolor(couleur)__|sélectionne la couleur de remplissage|
    |__up()__|monte le crayon: le tracé ne s'effectue plus|||
    
    __1. Dessiner__ un carré de 50 par 50 pixels, puis deux carrés adjacents de 80 par 80 pixels;

    __2. Dessiner__ un hexagone.
    {{ IDE('turtle') }}

    ??? done "Solution"
            
        Il faut chercher! :smile:

    <!--```python linenums="1"
        # Hexagone
        from turtle import *
        # Hexagone
        left(30)
        forward(50)
        right(60)
        forward(50)
        right(60)
        forward(50)
        right(60)
        forward(50)
        right(60)
        forward(50)
        right(60)
        forward(50)
        ```
        -->

[ ⬅️ Retour](../ch07){ .md-button }
[📄 Accueil](../../../){ .md-button }