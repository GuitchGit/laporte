[ ⬅️ Retour](../ch07){ .md-button }


=== "Séquence et affectation"

    ??? video

        |__Affectation__|Les séquences|
        |:---:|:---:|
        |<iframe width="400" height="226" src="https://www.youtube.com/embed/_jN8PWnqNks" title="Affectation en Python" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>|<iframe width="400" height="226" src="https://www.youtube.com/embed/Q_8t6oIUPRE" title="2.3 Les séquences" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>|
        |||
        |||

    # 1. Affectation

    Dans le langage Python, le symbole = correspondant à l'affectation.
    
    Pour __affecter__ la valeur 2 à une variable a, on écrit simplement a = 2.

    Ainsi, l'algorithme

    a ← 3
    
    b ← 2 * a

    s'écrit en Python de la façon suivante :

    a = 3
    
    b = 2 * a

    !!! warning "Attention"
        Le symbole = ici utilisé n'a rien à voir avec le symbole = utilisé en mathématique. On dit qu'on a affecté __`a`__ a la valeur 32, et il faut se représenter mentalement cette action par l'écriture a ← 32.

    ```python
    >>> a
    3
    >>> b
    6
    >>> c
    Traceback (most recent call last):
        File "<pyshell>", line 1, in <module>
    NameError: name 'c' is not defined
    ```

    ^^Remarque^^ : Il apparaît bien une erreur lorsqu'on a fait appel à une variable c qui n'avait jamais été définie, comme le dit explicitement le message
    
    __NameError: name 'c' is not defined__

    ???+ info "Définition"
        Un programme Python est un texte structuré comme une séquence d’instructions.
        
        Un interpréteur Python exécute le programme sur un ordinateur en mobilisant des ressources de calcul (processeur) et de mémoire.
        
        Après l’exécution d’une instruction, l’interpréteur évalue par défaut l’instruction sur la ligne suivante (les lignes vides ne sont pas prises en compte)mais certaines instructions se traduisent par des sauts en avant (branchement) ou en arrière (boucle) dans le texte du programme.
        
        Les séquences de caractères précédées d’un dièse # ne sont pas interprétées, ce sont des commentaires.

    En utilisant le terminal ci-dessous, __tester__ les commandes suivantes :
    ```python
    >>> 2+3
    
    >>> 2+*5

    >>> 2-1 * 5

    >>> (2-1)*5

    >>> 13 / 4

    >>> 13 // 4

    >>> 13 % 4

    >>> 5 / (2-2)

    >>> 2**10
    
    >>> 1,2 + 1,3
    
    ```
    {{ terminal() }}

    # 2. Les variables 
    
    Une variable est un emplacement doté d'un nom, utilisé pour stocker des données dans la mémoire.
    
    On peut considérer une variable comme un conteneur contenant des données pouvant être modifiées ultérieurement dans le programme.
    
    ???+ tldr "Exemples"
        ```python
        score = 203
        nom_joueur_1 = "mona-lisa"
        longueur = 1.25
        dimensions_valides = True
        ```
        ![](img/variables.png){: .center}
        
        Et voici des noms qui ne pourront pas être des identifiants de variables :

        - __`4e_personne`__ (commence par un chiffre),
        - __`Tic&Tac`__ (caractère & non autorisé),
        - __`joueur-2`__ (caractère - non autorisé),
        - __`prix ttc`__ (espace non autorisé),
        - __`global`__ (mot réservé)
        

    # 2.1. Existence d'une variable

    Une variable non créée ne peut pas être utilisée ! Par exemple, __tester__ la suite d'instructions :

    ```python
    valeur1 = 5.3
    valeur2 = 2.1 
    valeur1
    valeurs2
    valeur1 * valeur2
    valeur1valeur2
    ``` 
    {{ terminal() }}

    ???+ tip "Explications"
        Les variables __`valeur1`__ et __`valeur2`__ ont été définies donc peuvent être utilisées.
        
        Par contre les variables __`valeurs2`__ et __`valeur1valeur2`__ n'existent pas d'où les messages d'erreur.



    # 2.2. Le type d'une variable

    La valeur référencée par une variable n'est pas systématiquement une valeur numérique : dans les exemples précédent, on a référencé un entier (type __`int`__), un flottant (type __`float`__), une chaine de caractères (type __`str`__) et un booléen (type __`bool`__).
    
    En voici quelques uns, que nous découvrirons au fil de l'année :

    |Type Python| Traduction | Exemple|
    |:-:|:-:|:-:|
    |`int`|entier|`70`|
    |`float`|flottant (décimal)|`1.7306`|
    |`str`|chaîne de caractères (string)|`"NSI"`|
    |`bool`|booléen (True ou False)|False|
    |`tuple`|p-uplet| `(0, 255, 0)`|
    |`list`|liste|`[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]`|
    |`dict`|dictionnaire|`{'Tom': 16, 'Mathys':18, 'Guillaume':49, 'Yannis':10}`|
    |`function`|fonction| `print`|


    Comment connaître le type d'une variable ?
    Il suffit dans la console d'utiliser la fonction `type`.
    
    ```python
    >>> a = 1
    >>> type(a)
    <class 'int'>
    ```
        

    ???+ info "Pour aller plus loin"
        Python manipule des __objets__ qui sont référencés par des noms (Voir la programmation Objet en terminale) . Une variable est l’association entre un nom et un objet Python qu’on désigne souvent comme valeur de la variable.
        
        L’opérateur = réalise cette association. L’interpréteur Python évalue d’abord le membre de droite pour créer l’objet puis l’associe au membre de gauche 
        contenant le nom. Cette instruction s’appelle une __affectation__ de variable. Comme elle modifie l’état courant du programme, on parle __d’effet de bord__.

        
        |||
        |:---:|:---:|
        |![](img/Affectation.png){width=100% }|![](img/Affectation1.png){width=100%}|
        |![](img/Affectation2.png){width=100% align=right}|
        
    ???+ tip "Méthode"

        Pour bien comprendre un programme on peut compléter un tableau d’état : on exécute le programme (en fixant éventuellement une instance d’entrée) comme le ferait l’interpréteur, en notant les valeurs de toutes les variables pour chaque instruction qui une modifie l’état de la mémoire par effet de bord.

        Cet exemple classique permet de comprendre qu’il faut une variable de stockage pour sauvegarder la valeur de a si on veut échanger les valeurs des variables a et b

        En Python
        ```python linenums="1"
        a = 842
        b = 843
        a = b
        b = a
        ```

        |Instruction (numéro de ligne)|a|b|
        |:---:|:---:|:---:|
        |ligne 1| 842||
        |ligne 2| 842| 843|
        |ligne 3| 843| 843|
        |ligne 4| 843| 843|

        Pour échanger les valeurs des variables a et b on peut utiliser l’un des deux programme ci-dessous, le deuxième est plus pythonique utilise le déballage de tuple.
        
        En Python
        ```python linenums="1"
        a = 842
        b = 843
        c = a
        a = b
        b= c
        ```

        ```python linenums="1"
        a = 842
        b = 843
        a,b = b, a
        ```

    # 2.3. `Exemple d'algorithme : Variables et affectation`
    
    On veut calculer votre Indice de Masse Corporelle (IMC)
    
    On définit les variables `taille`et `poids` suivante, puis on calcule l'IMC:

    ```python linenums="1"
    taille = 1.78 # On initialise la taille à 1.78m
    poids = 70 # On initialise la taille à 70kg

    imc = poids / taille**2 # Calcul  de l'IMC
    ```

    Vous pouvez tester ci-dessous:

    {{ IDE()}}

    ???+ note "Remarques"
        - taille et poids sont ici des variables dites constantes
        
        - Les noms de variables en Python doivent obéir à certaines règles syntaxiques (ne pas commencer par un chiffre, ne peut pas contenir de tiret haut). Il est recommandé de les choisir en minuscules et d’utiliser le tiret bas comme séparateur si un nom est constitué de plusieurs mots comme `compteur_vie`. Le premier caractère est obligatoirement une lettre. 
        - Ces variables ont un type : taille est déclaré en 'float' et poids en 'int'
        - On constate que pour un nombre à virgule, on utilise le "." et non ","
        - imc est une nouvelle variable qui va donné l'indice de masse corporelle
        - **2 signifie au carré -> taille²

    # 2.4. L'identifiant d'une variable

    ## __`Choisir des noms explicites`__

    Dans l'algorithme précédent, les noms taille et poids sont explicites (On ne les a pas appelé __`a`__ ou __`b`__ ou que sais-je encore...). Par conséquent, en fonction des circonstances dans le code, il est recommandé d’utiliser des noms de variables indiquant leurs rôles.

    ## __`Eviter les accents et les caractères spéciaux`__

    Pour des raisons d'encodage de texte, je vous déconseille vivement d'utiliser des accents dans vos noms de variables ou de fichiers. Cela vous évitera bien des soucis...Les caractères spéciaux (@, ç,-,(,],...) sont à proscrire.

    ## __`Certains noms de variables ne sont pas valides`__

    - nous avons vu que certains mots (__`for`__, __`if`__, etc.) font partie du langage Python, évitez donc de les utiliser comme noms de variables. Ainsi l'instruction print = 30 redéfinit la fonction print en une variable print et vous ne pourriez plus alors utiliser la fonction print !! ;

    - un nom de variable ne peut pas commencer par un nombre, par exemple __`2pi = 6.28`__ n'est pas valide ;

    - un nom de variable ne peut pas comporter certains caractères, par exemple __`monmail@ = 'exemple@free.fr'`__ n'est pas valide.


    ??? info "Liste des mots-clés réservés par Python"

        <p align="center">
        <table>
            <tr><td>and</td><td>as </td><td>assert	</td><td>break</td><td>	class</td><td>	continue</td><td>	def</td><td>	del</td></tr> 
            <tr><td>elif</td><td>	else</td><td>	except</td><td> False </td><td> finally	</td><td>for</td><td>	from</td><td>	global  </td></tr>
            <tr> <td> if </td><td>	import</td><td>	in</td><td>	is	</td><td>lambda	</td><td>None </td><td>not </td><td>	or</td></tr>
            <tr><td> pass </td><td>raise</td><td>	return</td><td>	True </td><td>try	</td><td>while</td><td>	with	</td><td>yield </td></tr>
        </table>
        </p>

    ## __`Deux conventions d'écriture fréquemment utilisées`__

    Python respecte la casse : il fait une différence entre les caractères majuscules et minuscules.
    
    Par exemple, __`Temp`__, __`temp`__, __`TEMP`__ sont trois variables différentes. Pour limiter les erreurs, il est donc conseillé d'utiliser une des deux conventions suivantes :

    - __snake case__ (recommandée en Python) : toutes les lettres en minuscule et les mots séparés par un tiret bas _, par exemple __`temp_exterieure`__ ;

    - __camel case__ : la première lettre en minuscule puis des majuscules à chaque nouveau mot, par exemple __`tempExterieure`__.


=== "Instruction conditionnelle IF / ELIF / ELSE"

    ??? video

        |__Instructions conditionnelles__||
        |:---:|:---:|
        |<iframe width="400" height="226" src="https://www.youtube.com/embed/tjZQ64VjvaU" title="[Python] Si ... alors ... - partie 1" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>|<iframe width="400" height="226" src="https://www.youtube.com/embed/UlSmDST7zfk" title="[Python] Si ... alors ... - partie 2 - Entrée au clavier" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>|
        |<iframe width="400" height="226" src="https://www.youtube.com/embed/WNJRn4LbxXs" title="[Python] Si ... alors ... - partie 3 - Booléens" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>||
        |||


    # 1. Instructions conditionnelles : généralités

    Les instructions conditionnelles permettent de sélectionner le bloc d'instructions à exécuter selon la valeur d'une expression booléenne.

    ## `1.1. La structure IF`
    
    ```python
        if condition_1:
            bloc_instructions_1
        bloc_instruction_2
    ```
    ^^Exécution^^ : Si __`condition_1`__ est True (Vraie) alors __`bloc_instructions_1`__ est exécuté sinon __`bloc_instructions_2`__.

    ???+ tldr "Exemples - La condition est Vraie"

        ```python
        nb = 100 # 100 est un nombrer pair
        if nb % 2 == 0:
            print(nb, "est un nombre pair")
            print("fin des tests")

        >>> 100 est un nombre pair
        >>> fin des tests
        ```

        ^^Explication^^ : nb % 2 == 0 est une expression qui vaut True lorsque nb vaut 100. L'instruction print(nb, "est un nombre pair") est exécutée.
        
    ???+ tldr "Exemples - La condition est Fausse"

        ```python
        nb = 101 # 101 est un nombrer impair
        if nb % 2 == 0:
            print(nb, "est un nombre pair")
        print("fin des tests")
        >>> fin des tests
        ```

        ^^Explication^^ : nb % 2 == 0 est une expression qui vaut False lorsque nb vaut 101. L'instruction print(nb, "est un nombre pair") n'est pas exécutée.

        

    ## `1.2. La structure IF / ELSE`

    ```python
        if condition_1:
            bloc_instructions_1
        else:
            bloc_instruction_2
        bloc_instruction_3
    ```
    
    ^^Exécution^^ :  Si __`condition_1`__ est True (Vraie) alors __`bloc_instructions_1`__ est exécuté sinon __`bloc_instructions_2`__ puis on éxécute le __`bloc_instructions_3`__


    ???+ tldr "Exemples - La condition est Vraie"

        ```python
        age = 18 
        if age>=18 :
            print(f'Vous avez {age} ans et vous êtes majeur')
        else:
            print(f'Vous avez {age} ans et vous êtes mineur')
        print("Au revoir")
        
        >>> Vous avez 18 ans et vous êtes majeur
        >>> Au revoir
        ```

        ^^Explication^^ : age >= 18 est une expression qui vaut True lorsque age vaut 18. L'instruction print(f'Vous avez {age} ans et vous êtes majeur') et print("Au revoir") sont exécutées.
        
    ???+ tldr "Exemples - La condition est Fausse"

        ```python
        age = 5 
        if age>=18 :
            print(f'Vous avez {age} ans et vous êtes majeur')
        else:
            print(f'Vous avez {age} ans et vous êtes mineur')
        print("Au revoir")
        
        >>> Vous avez 5 ans et vous êtes mineur
        >>> Au revoir
        ```

       ^^Explication^^ : age >= 18 est une expression qui vaut False lorsque age vaut 5. L'instruction print(f'Vous avez {age} ans et vous êtes mineur') et print("Au revoir") sont exécutées.


    ## `1.3. La structure IF / ELIF / ELSE`

    ```python
        if condition1:
            bloc d’instructions1
        elif condition2:
            bloc d’instructions2
        elif condition3:
            bloc d’instructions3
        else:
            bloc d’instructionsN
    ```
    
    ^^Exécution^^ : 
    - Si condition1 est True (Vraie) alors bloc instructions1 est exécuté mais bloc d’instructions2, bloc instructions3 et bloc instructionsN sont ignorés.

    - Si condition1 est False (Faux) ET condition2 est True (Vraie) alors bloc instructions2 est exécuté mais bloc d’instructions1, bloc instructions3 et bloc instructionsN sont ignorés.

    - Si toutes les condition (1, 2, 3,...) sont False (Faux) alors bloc instructionsN est exécuté mais les autres bloc d'instructions (1, 2, 3,...) sont ignorés. 

    ???+ tldr "Exemple"

        ```python
        nb = 178
        if nb % 10 == 0:
            print(nb, "est un multiple de 10")
        elif nb % 5 == 0:
            print(nb, "est un multiple de 5, mais pas de 10")
        elif nb % 2 == 0:
            print(nb, "est un multiple de 2, mais pas de 10")
        else :
            print(nb, "n'est ni un multiple de 2, ni un multiple de 5")

        ```

    ^^Explication^^ : 178 est un multiple de 2, mais pas de 10

    nb % 10 == 0 est une expression qui vaut False lorsque nb vaut 178.
    
    L'instruction print(nb, "est un multiple de 10") est ignorée.

    nb % 5 == 0 est une expression qui vaut False lorsque nb vaut 178.

    L'instruction print(nb, "est un multiple de 5, mais pas de 10") est aussi ignorée.

    nb % 2 == 0 est une expression qui vaut True lorsque nb vaut 178. L'instruction print(nb, "est un multiple de 2, mais pas de 10") est donc exécutée.

    ## `2. Opérateurs booléens : Evaluation de la condition`

    L'expression qui suit le if est évaluée par Python lors de l'exécution du programme. Cette évaluation renvoie un booléen, True ou False.
    Les symboles de comparaison (ou d'appartenance) permettant d'écrire une condition sont :

    !!! info inline  "Opérateurs de comparaison"
        |Opérateur|Signification|
        |:-:|:-:|
        |`==`| est égal à|
        |`!=`|est différent de|
        |`<`|inférieur à|
        |`>`|supérieur à|
        |`<=`|inférieur ou égal à|
        |`>=`|supérieur ou égal à|
        |`in`| appartient à|
        |`not in`| n'appartient pas à|

    !!! note "Exemples"
        ```python
        >>> a = 2

        >>> a == 3
        False

        >>> a == 2
        True

        >>> a != 1
        True

        >>> a > 2
        False

        >>> a >= 2
        True

        >>> a <= 2
        True

        >>> a <= 5
        True

        >>> 'e' in 'abracadabra'
        False

        >>> 'b' in 'abracadabra'
        True

        >>> 'A' not in 'abracadabra'
        True

        >>> not True
        False





        ```
    

=== "Boucles bornées"
    
    ??? video

        |__Boucle FOR__||
        |:---:|:---:|
        |<iframe width="400" height="226" src="https://www.youtube.com/embed/4AY3YiduC14" title="cours python • boucle for ... in range(...) • programmation • tutoriel • lycée" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>||
        |||
        |||

    # 1. Généralités

    Les boucles bornées servent à exécuter certaines instructions d'un programme de manière répétitive, un nombre prédéfini de fois.

    Par exemple pour répéter quatre fois un bloc d'instructions, on peut écrire :

    ```python
        for i in range(5):
            bloc d'instructions
    ```

    ???+ tldr "Exemple"

        ```python
        for i in range(5):
            print("Bonjour!")

        Bonjour!
        Bonjour!
        Bonjour!
        Bonjour!
        Bonjour!
        ```

    Pour répéter n fois un bloc d'instructions, en première approche, on peut se contenter de la syntaxe :

    ```python
        for i in range(n):
            bloc d'instructions
    ```

    Mais les deux instructions que cette syntaxe combine : for ... in et range() permettent de nombreuses possibilités.

    # 2. La fonction __`range()`__

    Pour itérer sur une suite de nombres entiers, on peut utiliser la fonction __`range()`__.

    ???+ tldr "Exemple"

        ```python
        for i in range(5):
            print(f"A l'étape {i} on a i au carré égal à {i**2}")

        A l'étape 0 on a i au carré égal à 0
        A l'étape 1 on a i au carré égal à 1
        A l'étape 2 on a i au carré égal à 4
        A l'étape 3 on a i au carré égal à 9
        A l'étape 4 on a i au carré égal à 16
        ```
        Ici i prend successivement les valeurs 0, 1, 2, 3 puis 4 et pour chaque valeur de i, on affiche son carré.
    
    La syntaxe générale est __`range(debut, fin, pas)`__.

    La liste des valeurs générées commence avec la valeur __`debut`__; les valeurs suivantes sont générées en augmentant de la valeur __`pas`__ et la liste s'arrête juste avant d'atteindre ou dépasser la valeur __`fin`__.

    - __`debut`__ est un paramètre facultatif : sa valeur par défaut est __`0`__.

    - __`pas`__  est aussi un paramètre facultatif. Il vaut __`1`__ par défaut ;
    
    Il faut donner les 3 paramètres de __`range()`__ pour modifier sa valeur.

    ???+ tldr "Exemple"

        ```python
        range(10) génère la liste 0, 1, 2, 3, 4, 5, 6, 7, 8, 9

        range(6, 10) génère la liste 6, 7, 8, 9

        range(1, 12, 3) génère la liste 1, 4, 7, 10

        range(5, -10, -3) génère la liste 5, 2, -1, -4, -7
        ```

    !!! info "Info"

        L'objet renvoyé par __`range()`__ se comporte presque comme une liste, mais n'en est pas une.
        
        Cet objet génère les éléments de la séquence au fur et à mesure de l'itération, sans réellement produire la liste en tant que telle.
    
    Pour générer une liste, on peut utiliser les exemples suivants:

    ???+ tldr "Exemple"
    
        ```python
            # Les entiers de 0 (inclus) à 11 (exclu)
            liste_1 = list(range(11))
    
            # Les entiers de 0 (inclus) à 20 (exclu)
            liste_2 = list(range(20))

            # Les entiers de 0 (inclus) à 20 (inclus)
            liste_3 = list(range(21))

            # DEUX PARAMETRES #

            # Les entiers de 3 (inclus) à 10 (exlu)
            liste_4 = list(range(3, 10))

            # Les entiers de 12 (inclus) à 20 (inclus)
            liste_5 = list(range(12, 21))

            # Les entiers de -10 (inclus) à 1 (exlu)
            liste_6 = list(range(-10, 1))

            # Les entiers de -10 (inclus) à 1 (inclus)
            liste_7 = list(range(-10, 2))

            # TROIS PARAMETRES #

            # Les entiers de 20 (inclus) à 0 (exclu)
            liste_8 = list(range(20, -1, -1))

            # Les entiers de 500 (inclus) à 200 (exclu)
            liste_9 = list(range(500, 200, -1))

            # Les entiers de 10 (inclus) à -10 (exclu)
            liste_10 = list(range(10, -10, -1))

            # Les entiers de 25 (inclus) à -25 (inclus)
            liste_11 = list(range(25, -26, -1))

            # Les pairs de 1 (inclus) à 100 (exclu)
            liste_12 = list(range(1, 101, 2))

            # Les multiples de 3 de 0 (inclus) à 301 (exclu)
            liste_13 = list(range(1, 101, 2))

            # Les multiples de 7 de 0 (inclus) à 994 (inclus)
            liste_14 = list(range(1, 101, 2))

            # Les pairs de -100 (inclus) à 100 (exclu)
            liste_15 = list(range(-100, 101, 1))

            # Les multiples de 3 de 300 (inclus) à 102 (exclu)
            liste_16 = list(range(300, 101, -3))

            # Les multiples de 13 de 3445 (inclus) à -559 (inclus)
            liste_17 = list(range(300, 101, -3))

        ```
    # 3. L'instruction __`for ... in`__
    
    La boucle bornée (ou instruction __`for ... in`__) permet d’itérer sur les éléments d’une séquence (liste, chaine de caractères, etc.) dans l'ordre dans lequel les éléments apparaissent dans la séquence.

    ???+ tldr "Exemple : Si on itére dans une liste"

        ```python
        semaine = ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche']
        for jour in semaine:
            print(jour)
        
        Lundi
        Mardi
        Mercredi
        Jeudi
        Vendredi
        Samedi
        Dimanche
        ```

    !!! aide "Analyse grâce à PythonTutor"
    
        Cliquer sur "Click to Start Visualization" puis "Next" et observer bien l'évolution de la variable ```jour```.

        <iframe width="800" height="500" frameborder="0" src="https://pythontutor.com/iframe-embed.html#code=semaine%20%3D%20%5B'Lundi',%20'Mardi',%20'Mercredi',%20'Jeudi',%20'Vendredi',%20'Samedi',%20'Dimanche'%5D%0Afor%20jour%20in%20semaine%3A%0A%20%20%20%20print%28jour%29&codeDivHeight=400&codeDivWidth=350&cumulative=false&curInstr=0&heapPrimitives=nevernest&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false"> </iframe>

    La variable ```jour``` prend donc **successivement** tous les jours de la semaine de la liste  ```semaine```. 

    Pour chaque valeur de ```jour```, la ou les instruction(s) situées **de manière indentée** sous la ligne du ```for``` seront exécutées. 

    Ici, il y a simplement un ```print(jour)```, donc chaque jour de la liste ```"semaine"``` s'affiche l'un après l'autre.


    ???+ tldr "Exemple : Si on itére sur une chaine de caractère"

        ```python linenums='1'
        for lettre in 'NSI':
            print(lettre)
        ```
        va donner ceci :
        ```python
        N
        S
        I
        ```

    !!! aide "Analyse grâce à PythonTutor"
    
        
        Cliquer sur "Click to Start Visualization" puis "Next" et observer bien l'évolution de la variable ```lettre```. 

        <iframe width="800" height="500" frameborder="0" src="https://pythontutor.com/iframe-embed.html#code=for%20lettre%20in%20'NSI'%3A%0A%20%20%20%20print%28lettre%29&codeDivHeight=400&codeDivWidth=350&cumulative=false&curInstr=0&heapPrimitives=nevernest&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false"> </iframe>

    La variable ```lettre``` prend donc **successivement** toutes les lettres de la chaîne de caractère ```"NSI"```. 

    Pour chaque valeur de ```lettre```, la ou les instruction(s) situées **de manière indentée** sous la ligne du ```for``` seront exécutées. 

    Ici, il y a simplement un ```print(lettre)```, donc chaque lettre de ```"NSI"``` s'affiche l'une après l'autre.



    La syntaxe de la boucle for est :
    
    ```python
        for element in sequence:
            bloc d'instructions
    ```

    __`element`__ est une variable à laquelle il faut donner un nom, et qui prend successivement les valeurs des éléments de la séquence.
    
    Le bloc d'instructions à exécuter dans le corps de la boucle est à indenter.

    # 4. Itérer sur les indices d'une séquence
    
    Pour itérer sur les indices d'une séquence, on peut combiner les fonctions __`range()`__ et __`len()`__.
    
    La fonction __`len()`__ renvoie la longueur d'une séquence, c'est-à-dire son nombre d'éléments.

    ???+ tldr "Exemple"

        ```python
        semaine = ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche']
        for jour in range(len(semaine)):
            print(f'Le jour {jour+1} est le {semaine[jour]}')
        
        Le jour 1 est le Lundi
        Le jour 2 est le Mardi
        Le jour 3 est le Mercredi
        Le jour 4 est le Jeudi
        Le jour 5 est le Vendredi
        Le jour 6 est le Samedi
        Le jour 7 est le Dimanche
        ```

    Mais il existe une instruction spécifique pour cela : dans une boucle sur une séquence, l'indice et la valeur correspondante peuvent être récupérés en même temps en utilisant la fonction __`enumerate()`__.

    ???+ tldr "Exemple"

        ```python
        semaine = ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche']
        for indice, jour  in enumerate(semaine):
            print(f'Le jour {indice+1} est le {jour}')
        
        Le jour 1 est le Lundi
        Le jour 2 est le Mardi
        Le jour 3 est le Mercredi
        Le jour 4 est le Jeudi
        Le jour 5 est le Vendredi
        Le jour 6 est le Samedi
        Le jour 7 est le Dimanche
        ```
    # 5. Itérer sur plusieurs séquences

    Pour faire une boucle sur deux séquences ou plus en même temps, les éléments peuvent être associés en utilisant la fonction __`zip()`__ :

    ???+ tldr "Exemple"

        ```python
        couleurs =  ['anthracite', 'blanc', 'cyan']
        codes_hex = ['#303030', '#FFFFFF', '#00FFFF']
        for couleur, code in zip(couleurs, codes_hex):
             print(couleur, code)

        
        anthracite #303030
        blanc #FFFFFF
        cyan #00FFFF

        ```


=== "Boucles non bornées"

    ??? video

        |__Boucle WHILE__||
        |:---:|:---:|
        |<iframe width="400" height="226" src="https://www.youtube.com/embed/2Xxty4oaHBs" title="[Python] Boucle tant que I - partie 1" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>|<iframe width="400" height="226" src="https://www.youtube.com/embed/NRvv1odnoIU" title="[Python] Boucle tant que I - partie 2" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>|
        |||
        |||


    # 1. Généralités

    La boucle non bornée (ou boucle while, "tant que" en français) permet de répéter un bloc d'instructions tant qu'une expression booléenne est vraie.
    

    La syntaxe de la boucle __`while`__ est :

    Par exemple pour répéter quatre fois un bloc d'instructions, on peut écrire :

    ```python
        while expression
            bloc d'instructions à répéter
    ```
    où __`expression`__ a pour valeur True ou False.
    
    Attention : Le bloc d'instructions est à indenter.

    On utilise généralement cette boucle lorsqu'on ne connait pas le nombre d'itérations au préalable. Par exemple, si on joue à un jeu où l'on peut proposer 10 réponses maximales avant de perdre. On ne connait par avance quand la personne va trouver la bonne réponse ou pas d'ailleurs.

    À la différence essentielle des boucles for, dont on peut savoir à l'avance combien de fois elles vont être exécutées, les boucles while sont des boucles dont on ne sort que lorsqu'une condition n'est plus satisfaite.
    
    Avec donc le risque de rester infiniment bloqué à l'intérieur !
    
    ???+ tldr "Exemple 1 : Un compte à rebours"

        Soit le programme suivant:

        ```python linenums="1"
        a=5
        while a>-1:
            print(a)
            a-=1
        ```
                
    !!! aide "Analyse grâce à PythonTutor"
    
        
        Cliquer sur "Click to Start Visualization" puis "Next" et observer bien l'évolution de la variable ```lettre```. 

        <iframe width="800" height="350" frameborder="0" src="https://pythontutor.com/iframe-embed.html#code=a%3D5%0Awhile%20a%3E-1%3A%0A%20%20%20%20print%28a%29%0A%20%20%20%20a-%3D1&codeDivHeight=400&codeDivWidth=350&cumulative=false&curInstr=0&heapPrimitives=nevernest&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false"> </iframe>

        La variable ```a``` prend donc **successivement** les valeurs 5, 4, 3, 2, 1, 0
        
        ```a``` est décrémenté à chaque tour de boucle.

        Si on enlève la ligne 4, on rentre dans le problème courant des boucles while : La boucle infinie!

    ???+ tldr "Exemple 2"
        
        La boucle suivante permet d'afficher la plus petite puissance de 2 supérieure à nombre.
        On initialise la variable valeur à 1 (c'est à dire ), et tant qu'elle reste inférieure à nombre on la multiplie par 2. Dès que l'une des valeurs obtenues est supérieure ou égale à nombre on sort de la boucle et on l'affiche.
        
        ```python
        nombre = 999
        valeur = 1
        while valeur < nombre:
            valeur = valeur * 2
        print(valeur)
        ```

    !!! aide "Analyse grâce à PythonTutor"
    
        
        Cliquer sur "Click to Start Visualization" puis "Next" et observer bien l'évolution de la variable ```lettre```. 

       <iframe width="800" height="350" frameborder="0" src="https://pythontutor.com/iframe-embed.html#code=nombre%20%3D%20999%0Avaleur%20%3D%201%0Awhile%20valeur%20%3C%20nombre%3A%0A%20%20%20%20valeur%20%3D%20valeur%20*%202%0Aprint%28valeur%29&codeDivHeight=400&codeDivWidth=350&cumulative=false&curInstr=0&heapPrimitives=nevernest&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false"> </iframe>
        

    # Boucle infinie

    Comme vu précédemment, les boucles while présentent un risque de boucle infinie.
    
    Pour que la boucle prenne fin, il faut s'assurer que l'expression qui conditionne l'exécution du corps de la boucle prendra la valeur False, éventuellement après de très nombreuses étapes.
    
    Il faut donc s’assurer que la valeur de cette expression est bien modifiée par les instructions du corps de la boucle (ou par un événement).

    
    ???+ tldr "Exemple "
        
        La boucle suivante ne se termine jamais : la variable valeur n'est jamais modifiée dans le corps de la boucle. Seule une action de l’utilisateur peut interrompre l'exécution du programme.
        
        ```python
        compteur = 0
        valeur = 0
        while valeur == 0:
            compteur = compteur + 1
            print(compteur)
        ```

    À l'inverse, si l'expression qui conditionne l'exécution du corps de la boucle a pour valeur False dès le départ, le corps de la boucle n’est jamais exécuté.

    



=== "Appels de fonction"




<!--

    

    Considérons la liste `[5, 4, 2, 1]`  
    Voici le fonctionnement de l'algorithme :  
    ![](img/selection.gif){: .center}
    
    ## 2. Principe Tri Sélection
    
    !!! note "description de l'algorithme"
        Attention un tableau de 4 valeurs (4 cases) sera numéroté de 0 à 3 et non de 1 à 4

        Sur un tableau de n éléments (numérotés de 0 à n-1), le principe du __tri par sélection__ est le suivant :
        
        - __rechercher le plus petit élément du tableau, et l'échanger avec l'élément d'indice 0 ;__
        - __rechercher le second plus petit élément du tableau, et l'échanger avec l'élément d'indice 1 ;__
        - __continuer de cette façon jusqu'à ce que le tableau soit entièrement trié.__

        En pseudo-code, l'algorithme s'écrit ainsi :
            ```python 
            fonction tri_selection(tableau t):
                n ← longueur(t) 
                pour i de 0 à n - 2
                    indice_min ← i
                    pour j de i + 1 à n
                        si t[j] < t[indice_min], alors indice_min ← j
                    fin pour
                    si indice_min ≠ i, alors échanger t[i] et t[indice_min]
                fin pour
            fin fonction
            ```
 
    ## 3. Implémentation de l'algorithme Tri Sélection
    
    !!! abstract "Tri par sélection :heart: "
        ```python
        def tri_selection(tab) :
            n=len(tab)
            for i in range(n-2):
                indice_min = i
                for j in range(i+1, n) :
                    if tab[j] < tab[indice_min]:
                        indice_min = j
                # Inversion
                if indice_min!=i:
                    tmp = tab[i]
                    tab[i] = tab[indice_min]
                    tab[indice_min] = tmp
        ```
    *Vérification :*
    ```python
    >>> ma_liste = [7, 5, 2, 8, 1, 4]
    >>> tri_selection(ma_liste)
    >>> ma_liste
    [1, 2, 4, 5, 7, 8]
    ```

    Pour vérifier avec PythonTutor, cliquer [ici](https://urlz.fr/iB1n){target=__blank}

    ## 4. Complexité de l'algorithme
    ### 4.1 Mesure du temps d'exécution
    
    Nous allons fabriquer deux listes de taille 100 et 200 :

    ```python
    lst_a = [k for k in range(100,0,-1)] #on se place dans le pire des cas : une liste triée dans l'ordre décroissant

    lst_b = [k for k in range(200,0,-1)] #on se place dans le pire des cas : une liste triée dans l'ordre décroissant
    ```
    ??? info "On peut aussi utiliser la fonction random pour créer des listes de manières aléatoire"
        Ici on crée une liste de 10 chiffres compris entre 0 et 999
        ```python
        import random
        tab = [random.randint(0,1000) for i in range(10)]
        print(tab)
        ```

    La mesure du temps moyen de tri pour ces deux listes donne le résultat ci-dessous (avec le module ```timeit``` sous Jupyter Notebook)
    ```python
    %timeit tri_selection(lst_a)
    632 µs ± 14.3 µs per loop (mean ± std. dev. of 7 runs, 1000 loops each)
    ```
    
    ```python
    %timeit tri_selection(lst_b)
    2.35 ms ± 35.9 µs per loop (mean ± std. dev. of 7 runs, 100 loops each)
    ```
    ??? info "Voici l'algorithme à saisir sur un notebook Jupyter"
        ```python
        def tri_selection(tab) :
        n=len(tab)
        for i in range(n-2):
            indice_min = i
            for j in range(i+1, n) :
                if tab[j] < tab[indice_min]:
                    indice_min = j
            # Inversion
            if indice_min!=i:
                tmp = tab[i]
                tab[i] = tab[indice_min]
                tab[indice_min] = tmp

        lst_a = [k for k in range(100,0,-1)] #on se place dans le pire des cas : une liste triée dans l'ordre décroissant
        lst_b = [k for k in range(200,0,-1)] #on se place dans le pire des cas : une liste triée dans l'ordre décroissant
        print('Voici le temps de tri pour la liste lst_a')
        %timeit tri_selection(lst_a)
        print('Voici le temps de tri pour la liste lst_b')
        %timeit tri_selection(lst_b)
        ```
    En comparant les temps de tri des listes `lst_a` et `lst_b`, que pouvez-vous supposer sur la complexité du tri par sélection ?
    
    ??? done "Réponse"
        Une liste à trier 2 fois plus longue prend 4 fois plus de temps : l'algorithme semble de complexité **quadratique**.

        - Pire cas : $O(n^2)$
        - Moyenne : $O(n^2)$
        - Meilleur cas : $O(n^2)$

    ### 4.2. Calcul du nombre d'opérations
    Dénombrons le nombre d'opérations, pour une liste de taille $n$.

    - boucle `for` : elle s'exécute $n-1$ fois.
    - deuxième boucle `for` imbriquée : elle exécute d'abord 1 opération, puis 2, puis 3... jusqu'à $n-1$. 

    Or 
        $1+2+3+\dots+n-1=\dfrac{n \times (n-1)}{2}$

     Ceci est bien un polynôme du second degré, ce qui confirme que la complexité de ce tri est quadratique.

    **Vérification expérimentale**

    Insérer un compteur `c` dans votre algorithme pour vérifier le calcul précédent. On pourra renvoyer cette valeur en fin d'algorithme par un `return c`.

    ??? info "Voici l'algorithme à saisir sur un notebook Jupyter"
        ```python
        def tri_selection1(tab) :
            n=len(tab)
            c=0
            for i in range(n-2):
                indice_min = i
                for j in range(i+1, n) :
                    c+=1
                if tab[j] < tab[indice_min]:
                    indice_min = j
            # Inversion
            if indice_min!=i:
                tmp = tab[i]
                tab[i] = tab[indice_min]
                tab[indice_min] = tmp
        return c
        
        lst_a = [k for k in range(100,0,-1)] #on se place dans le pire des cas : une liste triée dans l'ordre décroissant
        tri_selection1(lst_a)
        lst_b = [k for k in range(200,0,-1)] #on se place dans le pire des cas : une liste triée dans l'ordre décroissant
        tri_selection1(lst_b)
        ```

    ## 5. Preuve de la correction de l'algorithme
    **Est-on sûr que notre algorithme va bien trier notre liste ?**
    
    Les preuves de correction sont des preuves théoriques.

    La preuve ici s'appuie sur le concept mathématique de **récurrence**. 

    Principe du raisonnement par récurrence : une propriété $P(n)$ est vraie si :    

    - $P(0)$ (par exemple) est vraie
    - Pour tout entier naturel $n$, si $P(n)$ est vraie alors $P(n+1)$ est vraie.
    
    Ici, la propriété serait : « Quand $i$ varie entre 0 et `longueur(liste) -2`, la sous-liste de longueur $i$ est triée dans l'ordre croissant.» On appelle cette
    propriété un **invariant de boucle** (sous-entendu : elle est vraie pour chaque boucle)        
    - quand $i$ vaut 0, on place le minimum de la liste en l[0], la sous-liste l[0] est donc triée.
    - si la sous-liste de $i$ éléments est triée, l'algorithme rajoute en dernière position de la liste le minimum de la sous-liste restante, dont tous les éléments sont supérieurs au maximum de la sous-liste de $i$ éléments. La sous-liste de $i+1$ éléments est donc aussi triée.

    ## 6. Preuve de la terminaison de l'algorithme
    **Est-on sûr que notre algorithme va s'arrêter ?**
    
    À l'observation du programme, constitué de deux boucles `for` imbriquées, il n'y a pas d'ambiguïté : on ne peut pas rentrer dans une boucle infinie. Le programme s'arrête forcément au bout de d'un nombre fixe d'opérations. 
    D'après nos calculs sur la complexité, ce nombre de tours de boucles est égal à $\dfrac{n \times (n-1)}{2}$.
    Ceci prouve que l'algorithme se terminera.

    ## 7. Bonus : comparaison des algorithmes de tri 
    Une démonstration du tri par sélection
    cliquer  [ici](http://mathartung.xyz/nsi/cours_algo_tri_selection.html){target=__blank}
    
    Une jolie animation permettant de comparer les tris :
    ![](img/comparaisons.gif){: .center}
    
    Issue de ce [site](https://www.toptal.com/developers/sorting-algorithms){target=__blank}

=== "Tri Insertion"

    ## 1. Animation Tri Insertion
    Considérons la liste `[7, 5, 2, 8, 1, 4]`   
    Voici le fonctionnement de l'algorithme :  
    ![](img/insertion1.gif){: .center}
    
    ## 2. Principe Tri Insertion

    !!! note "description de l'algorithme"
    - On traite successivement toutes les valeurs à trier, en commençant par celle en deuxième position.
    - Traitement : tant que la valeur à traiter est inférieure à celle située à sa gauche, on échange ces deux valeurs.

    
    ## 3. Implémentation de l'algorithme Tri Insertion
    
    !!! note "Tri par insertion (version simple) :heart:"
    ```python
    def tri_insertion1(tab):
        '''trie en place la liste tab donnée en paramètre'''
        for i in range(1, len(tab)):                 #(1)
            k = i                                    #(2)
            while k > 0 and tab[k-1] > tab[k] :      #(3)
                tab[k], tab[k-1] = tab[k-1], tab[k]  #(4)    
                k = k - 1                            #(5)   
    ```

    1. On commence à 1 et non pas à 0.
    2. On «duplique» la variable `i` en une variable `k`.  
    On se positionne sur l'élément d'indice ```k```. On va faire «reculer» cet élément tant que c'est possible. On ne touche pas à ```i```. 
    3. Tant qu'on n'est pas revenu au début de la liste et qu'il y a une valeur plus grande à gauche.
    4. On échange de place avec l'élément précédent.
    5. Notre élément est maintenant à l'indice ```k - 1```.  
    La boucle peut continuer.

    Pour vérifier sur PythonTutor, cliquer [ici](https://urlz.fr/izmt){target=__blank}

    *Vérification :*

    ```python
    >>> maliste = [7, 5, 2, 8, 1, 4]
    >>> tri_insertion1(maliste)
    >>> maliste
    [1, 2, 4, 5, 7, 8]
    ```
-->
    
[ ⬅️ Retour](../ch07){ .md-button }
[📄 Accueil](../../../){ .md-button }


