# Je m'entrîne encore et encore...

<! --
 Création d'un lien vers un nouvel onglet {:target="_blank" }  
[Aide pour creer Mkdocs](https://ens-fr.gitlab.io/mkdocs/){:target="_blank" } 
[recherche Icons](https://fontawesomeicons.com/svg/icons/pip){:target="_blank" }
-->



!!! note "Niveau Débutant"
    - [^^e-nsi^^](https://e-nsi.gitlab.io/pratique/tags/#0-simple){:target="_blank"}
    - [^^Futurtecoder^^](https://fr.futurecoder.io/course/#toc){:target="_blank" }

!!! note "Niveau Intermédiaire"
    - [^^France IOI^^](http://www.france-ioi.org/){:target="_blank" }
        

!!! note "Niveau Professionnel lol"


[📄 Accueil](../../../){ .md-button }


    

